#!/bin/bash

SCRIPTS_DIR=`dirname $0`

. "${SCRIPTS_DIR}/common.sh"

"${SCRIPTS_DIR}/build.sh" --rebuild

cd ${BUILD_DIR}

pvs-studio-analyzer analyze \
  --exclude-path ${PROJECT_ROOT}/lab-10/contrib/parson \
  -o "${BUILD_DIR}/report.log"

plog-converter --indicate-warnings -t fullhtml "${BUILD_DIR}/report.log" \
  -o "${BUILD_DIR}/report.html"
