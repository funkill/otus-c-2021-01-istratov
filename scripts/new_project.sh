#!/bin/bash

SCRIPTS_DIR=`dirname $0`

. "${SCRIPTS_DIR}/common.sh"

PROJECT_NAME="${1}"

mkdir -p "${PROJECT_ROOT}/${PROJECT_NAME}"

cat <<EOF > "${PROJECT_ROOT}/${PROJECT_NAME}/meson.build"
executable('${PROJECT_NAME}', ['main.c', 'lib.c'])
EOF

cat <<EOF > "${PROJECT_ROOT}/${PROJECT_NAME}/main.c"
// This is a personal academic project. Dear PVS-Studio, please check it.

// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include <stdio.h>

#include "lib.h"

int main(int argc, char **argv) {
    printf("Hello world!\n");

    return 0;
}
EOF

cat <<EOF > "${PROJECT_ROOT}/${PROJECT_NAME}/lib.c"
// This is a personal academic project. Dear PVS-Studio, please check it.

// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include "lib.h"
EOF

cat <<EOF > "${PROJECT_ROOT}/${PROJECT_NAME}/lib.h"
#ifndef __LIB_C
#define __LIB_C

#endif // __LIB_C
EOF

echo "subdir('${PROJECT_NAME}')" >> "${PROJECT_ROOT}/meson.build"
