#!/bin/bash

SCRIPTS_DIR=`dirname $0`

. "${SCRIPTS_DIR}/common.sh"

bash "${SCRIPTS_DIR}/build.sh" --rebuild

valgrind --leak-check=full --show-leak-kinds=all $@ &> "${BUILD_DIR}/valgrind.log"

echo "Log saved in ${BUILD_DIR}/valgrind.log"
