#!/bin/bash

SCRIPTS_DIR=`dirname $0`

. "${SCRIPTS_DIR}/common.sh"

MESON_OPTS=""

function parse_opts {
  while true; do
    if [ $# -eq 0 ]; then
      break;
    fi

    case "$1" in
      "--")
        ;;
      "--rebuild")
        MESON_OPTS="${MESON_OPTS} --wipe"
        ;;
      "--help"|"-h")
        echo "$0 [--rebuild] | --help | -h"
        exit 0;
        ;;
    esac

    shift
  done
}

parse_opts $@

meson "${BUILD_DIR}" ${MESON_OPTS} && \
    meson compile -C "${BUILD_DIR}"
