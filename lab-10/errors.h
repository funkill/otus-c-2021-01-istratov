#ifndef __ERRORS_H
#define __ERRORS_H

#define SET_ERRNO(no) { if (0 == errno) {errno = no;} }

typedef enum {
    INVALID_ARGS = 1024,
    MEMORY_ALLOCATION_ERROR,
    CURL_INIT_ERROR,
    CURL_REQUEST_ERROR,
    JSON_INVALID_FORMAT,
    CONVERTATION_ERROR,
    NOT_IMPLEMENTED,
    LOCATION_NOT_FOUND,
    CURL_NOT_HANDLED_HTTP_CODE,
} Errors;

char *describe_error(Errors);

#endif // __ERRORS_H