#ifndef __ARGS_H
#define __ARGS_H

typedef struct args_t {
    char *place;
    float *latitude;
    float *longitude;
} args;

args *parse_args(int argc, char **argv);
void args_deinit(args *);

#endif // __ARGS_H
