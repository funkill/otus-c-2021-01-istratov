// This is a personal academic project. Dear PVS-Studio, please check it.

// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include <errno.h>
#include <string.h>

#include "errors.h"

char *describe_error(Errors error) {
    switch (error) {
        case INVALID_ARGS: return "Invalid arguments error";
        case MEMORY_ALLOCATION_ERROR: return "Couldn't allocate memory";
        case CURL_INIT_ERROR: return "Couldn't initialize curl";
        case CURL_REQUEST_ERROR: return "Request returns non 200 code";
        case LOCATION_NOT_FOUND: return "Location not found";
        case JSON_INVALID_FORMAT: return "Received JSON has invalid format";
        case CONVERTATION_ERROR: return "Convertation failed";
        case NOT_IMPLEMENTED: return "Not implemented";
        case CURL_NOT_HANDLED_HTTP_CODE: return "HTTP code not handled";
        default: {
            return strerror(errno);
        }
    }
}