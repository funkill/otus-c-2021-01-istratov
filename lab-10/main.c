// This is a personal academic project. Dear PVS-Studio, please check it.

// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>

#include "args.h"
#include "errors.h"
#include "lib.h"

int main(int argc, char **argv) {
    args *args = parse_args(argc, argv);

    int errno;
    MetaWeather *mw = mw_init();
    if (NULL == mw) {
        args_deinit(args);
        printf("Error: %s\n", describe_error((Errors)errno));
        exit(errno);
    }

    Weather *w = NULL;
    if (NULL != args->place) {
        w = mw_get_for_place(mw, args->place);
    } else { // Always valid because we check it before return args.
        w = mw_get_for_location(mw, *args->latitude, *args->longitude);
    }

    if (NULL == w) {
        mw_deinit(mw);
        args_deinit(args);
        printf("Error %d: %s\n", errno, describe_error((Errors)errno));
        exit(errno);
    }

    printf(
        "Date: %s\n"
        "Speed of wind: %f\n"
        "Direction of wind: %s\n"
        "Min temp/Max temp: %f/%f\n",
        w->consolidated_weather->applicable_date,
        w->consolidated_weather->wind_speed,
        w->consolidated_weather->wind_direction_compass,
        w->consolidated_weather->min_temp,
        w->consolidated_weather->max_temp
    );

    weather_deinit(w);
    mw_deinit(mw);
    args_deinit(args);

    return 0;
}
