// This is a personal academic project. Dear PVS-Studio, please check it.

// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include <assert.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <curl/curl.h>

#include "contrib.h"
#include "errors.h"
#include "lib.h"

#define METAWEATHER_DOMAIN "https://www.metaweather.com/api"
#define METAWEATHER_LOCATION_URL METAWEATHER_DOMAIN "/location"
#define METAWEATHER_SEARCH_URL METAWEATHER_LOCATION_URL "/search/"

typedef struct buffer_t {
    char *mem;
    size_t size;
} buffer;

struct MetaWeather_t {
    CURL *curl;
};

MetaWeather *mw_init() {
    MetaWeather *mw = malloc(sizeof(MetaWeather));
    if (NULL == mw) {
        SET_ERRNO(MEMORY_ALLOCATION_ERROR);

        return mw;
    }

    CURL *ch = curl_easy_init();
    if (NULL == ch) {
        SET_ERRNO(CURL_INIT_ERROR);

        return mw;
    }

    mw->curl = ch;

    return mw;
}

void mw_deinit(MetaWeather *self) {
    assert(NULL != self);

    curl_easy_cleanup(self->curl);
    free(self);
}

static size_t mw_collect_response_callback(void *contents, size_t length, size_t nmemb, void *userp) {
    size_t realsize = length * nmemb;
    buffer *buf = (struct buffer_t *)userp;

    char *mem = realloc(buf->mem, buf->size + realsize);
    if (NULL == mem) {
        return 0;
    }

    buf->mem = mem;
    memmove(buf->mem + buf->size, contents, realsize);
    buf->size += realsize;

    return realsize;
}

Weather *mw_get_weather_for_woeid(MetaWeather *self, int woeid) {
    #define WOEID_LEN 8
    char woeid_s[WOEID_LEN];

    int woeid_len = snprintf(woeid_s, WOEID_LEN, "%d", woeid);
    if (woeid_len < 0) {
        SET_ERRNO(CONVERTATION_ERROR);

        return NULL;
    }

    int url_len = strlen(METAWEATHER_LOCATION_URL "/") + woeid_len + 1;
    char *url = calloc(url_len, sizeof(char));
    if (NULL == url) {
        SET_ERRNO(MEMORY_ALLOCATION_ERROR);

        return NULL;
    }

    strncat(url, METAWEATHER_LOCATION_URL "/", url_len);
    strncat(url, woeid_s, url_len);
    curl_easy_reset(self->curl);
    curl_easy_setopt(self->curl, CURLOPT_WRITEFUNCTION, mw_collect_response_callback);
    curl_easy_setopt(self->curl, CURLOPT_FOLLOWLOCATION, 1);

    curl_easy_setopt(self->curl, CURLOPT_URL, url);
    buffer buffer = { NULL, 0 };
    curl_easy_setopt(self->curl, CURLOPT_WRITEDATA, &buffer);
    CURLcode res = curl_easy_perform(self->curl);
    if (CURLE_OK != res) {
        free(url);
        free(buffer.mem);

        SET_ERRNO(CURL_REQUEST_ERROR);

        return NULL;
    }

    free(url);

    Weather *weather = weather_try_from_string(buffer.mem);

    free(buffer.mem);

    return weather;
}

int extract_woeid_from_search_response(JSON_Value *val) {
    JSON_Array *arr;
    if (NULL == (arr = json_value_get_array(val))) {
        SET_ERRNO(JSON_INVALID_FORMAT);

        return -1;
    }

    if (0 == json_array_get_count(arr)) {
        SET_ERRNO(LOCATION_NOT_FOUND);

        return -1;
    }

    JSON_Object *obj;
    if (NULL == (obj = json_array_get_object(arr, 0))) {
        SET_ERRNO(JSON_INVALID_FORMAT);

        return -1;
    }

    if (0 == json_object_has_value(obj, "woeid")) {
        SET_ERRNO(JSON_INVALID_FORMAT);

        return -1;
    }

    JSON_Value *woeid_j;
    if (NULL == (woeid_j = json_object_get_value(obj, "woeid"))) {
        SET_ERRNO(JSON_INVALID_FORMAT);

        return -1;
    }

    if (JSONNumber != json_value_get_type(woeid_j)) {
        SET_ERRNO(JSON_INVALID_FORMAT);

        return -1;
    }

    return (int)json_value_get_number(woeid_j);
}

Weather *mw_get_for_url(MetaWeather *self, char *url) {
    int errno;

    curl_easy_setopt(self->curl, CURLOPT_WRITEFUNCTION, mw_collect_response_callback);

    curl_easy_setopt(self->curl, CURLOPT_URL, url);
    buffer buffer = { NULL, 0 };
    curl_easy_setopt(self->curl, CURLOPT_WRITEDATA, &buffer);

    CURLcode result = curl_easy_perform(self->curl);
    if (CURLE_OK != result) {
        free(buffer.mem);

        SET_ERRNO(CURL_REQUEST_ERROR);

        return NULL;
    }

    long httpcode;
    curl_easy_getinfo(self->curl, CURLINFO_RESPONSE_CODE, &httpcode);
    if (200 != httpcode) {
        switch (httpcode) {
            case 404: {
                free(buffer.mem);
                SET_ERRNO(LOCATION_NOT_FOUND);
                return NULL;
            }
            default: {
                free(buffer.mem);
                SET_ERRNO(CURL_NOT_HANDLED_HTTP_CODE);
                return NULL;
            }
        }
    }

    JSON_Value *val = json_parse_string(buffer.mem);
    if (NULL == val) {
        free(buffer.mem);

        SET_ERRNO(JSON_INVALID_FORMAT);

        return NULL;
    }

    int woeid_i = extract_woeid_from_search_response(val);
    if (-1 == woeid_i) {
        free(buffer.mem);
        json_value_free(val);

        SET_ERRNO(JSON_INVALID_FORMAT);

        return NULL;
    }

    free(buffer.mem);
    json_value_free(val);

    return mw_get_weather_for_woeid(self, woeid_i);
}

Weather *mw_get_for_place(MetaWeather *self, char *location) {
    int errno;

    int url_len = strlen(METAWEATHER_SEARCH_URL "?query=") + strlen(location) + 1;
    char *url = calloc(url_len, sizeof(char));
    if (NULL == url) {
        SET_ERRNO(MEMORY_ALLOCATION_ERROR);

        return NULL;
    }

    strncat(url, METAWEATHER_SEARCH_URL "?query=", url_len);
    strncat(url, location, url_len);
    // url[url_len] = '\0'; not nedded because we used calloc

    Weather *weather = mw_get_for_url(self, url);

    free(url);

    return weather;
}

Weather *mw_get_for_location(MetaWeather *self, float latitude, float longitude) {
    #define LAT_LONG_LEN 10
    int errno;

    char lat_s[LAT_LONG_LEN];
    char long_s[LAT_LONG_LEN];
    int lat_len = snprintf(lat_s, LAT_LONG_LEN, "%f", latitude);
    int long_len = snprintf(long_s, LAT_LONG_LEN, "%f", longitude);
    int url_len = strlen(METAWEATHER_SEARCH_URL "?lattlong=") + lat_len + long_len + 1;
    char *url = calloc(url_len, sizeof(char *));
    if (NULL == url) {
        SET_ERRNO(MEMORY_ALLOCATION_ERROR);

        return NULL;
    }

    strncat(url, METAWEATHER_SEARCH_URL "?lattlong=", url_len);
    strncat(url, lat_s, url_len);
    strncat(url, ",", url_len);
    strncat(url, long_s, url_len);

    Weather *weather =  mw_get_for_url(self, url);

    free(url);

    return weather;
}
