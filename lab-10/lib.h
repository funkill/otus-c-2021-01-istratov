#ifndef __LIB_C
#define __LIB_C

#include "weather.h"

typedef struct MetaWeather_t MetaWeather;

MetaWeather *mw_init();
Weather *mw_get_for_place(MetaWeather *w, char *location);
Weather *mw_get_for_location(MetaWeather *w, float latitude, float longitude);
void mw_deinit(MetaWeather *w);

#endif // __LIB_C
