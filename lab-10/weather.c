// This is a personal academic project. Dear PVS-Studio, please check it.

// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include <assert.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "contrib.h"
#include "errors.h"
#include "weather.h"

int extract_string(JSON_Object *from, char *name, char **to) {
    assert(NULL != to);
    assert(NULL != name);
    assert(NULL != from);

    int len = json_object_get_string_len(from, name);
    char *val = (char *)json_object_get_string(from, name);
    if (NULL == val) {
        SET_ERRNO(JSON_INVALID_FORMAT);

        return JSON_INVALID_FORMAT;
    }

    char *val_s = malloc(len + 1);
    if (NULL == val_s) {
        free(val);
        SET_ERRNO(MEMORY_ALLOCATION_ERROR);

        return MEMORY_ALLOCATION_ERROR;
    }

    *to = strncpy(val_s, val, len);
    (*to)[len] = '\0';

    return 0;
}

int extract_number(JSON_Object *from, char *name, double *to) {
    assert(NULL != to);
    assert(NULL != name);
    assert(NULL != from);

    if (!json_object_has_value(from, name)) {
        SET_ERRNO(JSON_INVALID_FORMAT);

        return JSON_INVALID_FORMAT;
    }

    JSON_Value *val;
    if (NULL == (val = json_object_get_value(from, name))) {
        SET_ERRNO(JSON_INVALID_FORMAT);

        return JSON_INVALID_FORMAT;
    }

    if (JSONNumber != json_value_get_type(val)) {
        SET_ERRNO(JSON_INVALID_FORMAT);

        return JSON_INVALID_FORMAT;
    }

    *to = json_value_get_number(val);

    return 0;
}

int extract_lu(JSON_Object *from, char *name, unsigned long *to) {
    double number;
    int err = extract_number(from, name, &number);
    if (0 != err) {
        return err;
    }

    *to = (unsigned long)number;

    return 0;
}

int extract_li(JSON_Object *from, char *name, unsigned int *to) {
    double number;
    int err = extract_number(from, name, &number);
    if (0 != err) {
        return err;
    }

    *to = (unsigned int)number;

    return 0;
}

int extract_float(JSON_Object *from, char *name, float *to) {
    double number;
    int err = extract_number(from, name, &number);
    if (0 != err) {
        return err;
    }

    *to = (float)number;

    return 0;
}

WeatherItem *weather_item_init() {
    return calloc(1, sizeof(WeatherItem));
}

void weather_item_deinit(WeatherItem *self) {
    #define RELEASE_IF_NOT_NULL(item) { \
        if (NULL != item) {             \
            free(item);                 \
            item = NULL;                \
        }                               \
    }

    RELEASE_IF_NOT_NULL(self->applicable_date);
    RELEASE_IF_NOT_NULL(self->weather_state_abbr);
    RELEASE_IF_NOT_NULL(self->weather_state_name);
    RELEASE_IF_NOT_NULL(self->wind_direction_compass);

    free(self);
}

WeatherItem *weather_item_try_from_object(JSON_Object *obj) {
    #define FAIL_ON_NON_ZERO(val) {     \
        int ret = val;                  \
        if (0 != ret) {                 \
            weather_item_deinit(self);  \
            return NULL;                \
        }                               \
    }

    WeatherItem *self = weather_item_init();
    if (NULL == self) {
        SET_ERRNO(MEMORY_ALLOCATION_ERROR);

        return NULL;
    }

    FAIL_ON_NON_ZERO(extract_lu(obj, "id", &self->id));
    FAIL_ON_NON_ZERO(extract_string(obj, "applicable_date", &self->applicable_date));
    FAIL_ON_NON_ZERO(extract_string(obj, "weather_state_name", &self->weather_state_name));
    FAIL_ON_NON_ZERO(extract_string(obj, "weather_state_abbr", &self->weather_state_abbr));
    FAIL_ON_NON_ZERO(extract_string(obj, "wind_direction_compass", &self->wind_direction_compass));
    FAIL_ON_NON_ZERO(extract_float(obj, "wind_speed", &self->wind_speed));
    FAIL_ON_NON_ZERO(extract_float(obj, "wind_direction", &self->wind_direction));
    FAIL_ON_NON_ZERO(extract_float(obj, "min_temp", &self->min_temp));
    FAIL_ON_NON_ZERO(extract_float(obj, "max_temp", &self->max_temp));
    FAIL_ON_NON_ZERO(extract_float(obj, "the_temp", &self->the_temp));
    FAIL_ON_NON_ZERO(extract_float(obj, "air_pressure", &self->air_pressure));
    FAIL_ON_NON_ZERO(extract_float(obj, "humidity", &self->humidity));
    FAIL_ON_NON_ZERO(extract_float(obj, "visibility", &self->visibility));
    FAIL_ON_NON_ZERO(extract_li(obj, "predictability", &self->predictability));

    return self;
}

Weather* weather_init() {
    Weather *self = malloc(sizeof(Weather));
    if (NULL != self) {
        self->consolidated_weather = NULL;
    }

    return self;
}

void weather_deinit(Weather *self) {
    if (NULL != self->consolidated_weather) {
        weather_item_deinit(self->consolidated_weather);
        self->consolidated_weather = NULL;
    }

    free(self);
}

int try_fill_from_json(Weather **self, JSON_Object *obj) {
    int errno;

    JSON_Array *cw = json_object_get_array(obj, "consolidated_weather");
    if (NULL == cw) {
        SET_ERRNO(JSON_INVALID_FORMAT);

        return JSON_INVALID_FORMAT;
    }

    JSON_Object *cw_item = json_array_get_object(cw, 0);
    if (NULL == cw_item) {
        SET_ERRNO(JSON_INVALID_FORMAT);

        return JSON_INVALID_FORMAT;
    }

    WeatherItem *item = weather_item_try_from_object(cw_item);
    if (NULL == item) {
        SET_ERRNO(JSON_INVALID_FORMAT);

        return JSON_INVALID_FORMAT;
    }

    (*self)->consolidated_weather = item;

    return 0;
}

Weather *weather_try_from_string(char *str) {
    int errno;
    Weather *self = weather_init();
    if (NULL == self) {
        SET_ERRNO(MEMORY_ALLOCATION_ERROR);

        return NULL;
    }

    JSON_Value *val = json_parse_string(str);
    if (NULL == val) {
        weather_deinit(self);

        SET_ERRNO(JSON_INVALID_FORMAT);

        return NULL;
    }

    JSON_Object *obj;
    if (NULL == (obj = json_value_get_object(val))) {
        weather_deinit(self);
        json_value_free(val);

        SET_ERRNO(JSON_INVALID_FORMAT);

        return NULL;
    }

    if (0 != try_fill_from_json(&self, obj)) {
        weather_deinit(self);
        json_value_free(val);

        return NULL;
    }

    json_value_free(val);

    return self;
}