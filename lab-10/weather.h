#ifndef __WEATHER_H
#define __WEATHER_H

typedef struct WeatherItem_t {
    unsigned long id;
    char *applicable_date;
    char *weather_state_name;
    char *weather_state_abbr;
    float wind_speed;
    float wind_direction;
    char *wind_direction_compass;
    float min_temp;
    float max_temp;
    float the_temp;
    float air_pressure;
    float humidity;
    float visibility;
    unsigned int predictability;
} WeatherItem;

// typedef enum {
//     CITY = 1,
//     REGION, // Region / State / Province
//     COUNTRY,
//     CONTINENT,
// } LocationType;

// typedef struct Parent_t {
//     char *title;
//     LocationType location_type;
//     char *latt_long;
//     unsigned int woeid;
// } Parent;

typedef struct Weather_t {
    WeatherItem *consolidated_weather;
    // char *time;
    // char *sun_rise;
    // char *sun_set;
    // char *timezone_name;
    // Parent *parent;
    // Source *sources;
    // char *title;
    // LocationType location_type;
    // unsigned int woeid;
    // char *latt_long;
    // char *timezone;
} Weather;

Weather *weather_try_from_string(char *);
void weather_deinit(Weather *);

#endif // __WEATHER_H