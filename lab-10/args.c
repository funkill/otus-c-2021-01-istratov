// This is a personal academic project. Dear PVS-Studio, please check it.

// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include <assert.h>
#include <stdio.h>
#include <stdbool.h>
#include <getopt.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h> // include function strcasecmp on linux
#include <errno.h>
#include <math.h>

#include "args.h"
#include "errors.h"

void print_usage() {
    printf("weather meter: simple detector of weather\n"
        "Usage: main -p <place> | -a <latitude> -o <longitude>\n"
        "Options:\n"
        "  -p <place>            name of place\n"
        "  -a <latitude>         lAtitude\n"
        "  -o <longitude>        lOngitude\n"
        "As parameters can be place ot latitude/longitude pair\n"
        "\n"
        "For bug reporting instructions, please see:\n"
        "<https://gitlab.com/funkill/otus-c-2021-01-istratov/-/issues>.\n"
    );
}

args *args_unitialized() {
    return calloc(1, sizeof(args));
}

void args_deinit(args *self) {
    #define RELEASE_IF_NOT_NULL(val) {  \
        if (NULL != val) {              \
            free(val);                  \
            val = NULL;                 \
        }                               \
    }

    RELEASE_IF_NOT_NULL(self->latitude);
    RELEASE_IF_NOT_NULL(self->longitude);

    free(self);
    self = NULL;
}

bool args_validate(args *self) {
    assert(NULL != self);

    return (NULL != self->place) || (NULL != self->latitude && NULL != self->longitude);
}

// Function always return args or terminate program
args *parse_args(int argc, char **argv) {
    args *self = args_unitialized();
    if (NULL == self) {
        printf("Failed to allocate arguments\n");
        exit(MEMORY_ALLOCATION_ERROR);
    }

    int opt;
    while ((opt = getopt(argc, argv, "p:o:a:h")) != -1) {
        switch (opt) {
            case 'p':
                self->place = optarg;
                break;
            case 'o': {
                errno = 0;
                char *tail;
                float val = strtof(optarg, &tail);
                if (errno || strncmp("", tail, 1) || isnan(val) || isinf(val)) {
                    printf("Invalid longitude\n");
                    exit(INVALID_ARGS);
                }

                self->longitude = malloc(sizeof(float));
                if (NULL == self->longitude) {
                    printf("Failed allocate memory for longitude\n");
                    exit(MEMORY_ALLOCATION_ERROR);
                }

                *self->longitude = val;

                break;
            }
            case 'a': {
                errno = 0;
                char *tail;
                float val = strtof(optarg, &tail);
                if (errno || strncmp("", tail, 1) || isnan(val) || isinf(val)) {
                    printf("Invalid latitude\n");
                    exit(INVALID_ARGS);
                }

                self->latitude = malloc(sizeof(float));
                if (NULL == self->latitude) {
                    printf("Failed allocate memory for latitude\n");
                    exit(MEMORY_ALLOCATION_ERROR);
                }

                *self->latitude = val;

                break;
            }
            case 'h':
                print_usage();
                exit(0);
                break;
        }
    }

    if (!args_validate(self)) {
        print_usage();
        exit(INVALID_ARGS);
    }

    return self;
}
