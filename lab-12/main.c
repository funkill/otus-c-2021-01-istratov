// This is a personal academic project. Dear PVS-Studio, please check it.

// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

#include "logger.h"
#include "logger_handler.h"

int main(int argc, char **argv) {
    assert(argc > 0);
    assert(NULL != argv);

    Logger *logger = logger_init_empty();
    if (NULL == logger) {
        printf("Logger not initalized\n");
        exit(1);
    }

    ConsoleHandler *ch = console_handler_init(DEBUG);
    logger_push_handler(logger, (LoggerHandler *)ch);

    FileHandler *fh = file_handler_init(INFO, "file.log");
    logger_push_handler(logger, (LoggerHandler *)fh);

    SyslogHandler *sh = syslog_handler_init(0, 0, INFO);
    logger_push_handler(logger, (LoggerHandler *)sh);

    logger_debug("debug message");
    logger_info("info message");
    logger_warn("warn message");
    logger_error("error message");
    logger_crit("crit message");

    logger_deinit(logger);

    return 0;
}
