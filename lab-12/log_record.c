// This is a personal academic project. Dear PVS-Studio, please check it.

// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include <assert.h>
#include <stdlib.h>

#include "bt.h"
#include "log_record.h"

LogRecord *log_record_init(LogLevel level, char *message, char *file, int line, Backtrace *additional_data) {
    assert(NULL != message);
    assert(NULL != file);

    LogRecord *self = calloc(sizeof(LogRecord), 1);
    if (NULL != self) {
        self->level = level;
        self->message = message;
        self->file = file;
        self->line = line;
        self->additional_data = additional_data;
    }

    return self;
}

void log_record_deinit(LogRecord *self) {
    if (NULL != self->additional_data) {
        bt_deinit(self->additional_data);
    }

    free(self);
}