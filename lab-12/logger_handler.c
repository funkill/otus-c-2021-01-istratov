// This is a personal academic project. Dear PVS-Studio, please check it.

// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include <assert.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <syslog.h>

#include "bt.h"
#include "logger_handler.h"

LoggerHandler logger_handler_init(
    bool((*can_handle)(LoggerHandler *, LogLevel)),
    void((*handle_record)(LoggerHandler *, LogRecord **)),
    void((*release)(LoggerHandler *))
) {
    LoggerHandler self;
    self.can_handle = can_handle;
    self.handle_record = handle_record;
    self.release = release;

    return self;
}

struct ConsoleHandler {
    LoggerHandler parent;
    LogLevel max_level;
};


bool console_logger_can_handle(LoggerHandler *self, LogLevel level) {
    ConsoleHandler *ch = (ConsoleHandler *)self;

    return level >= ch->max_level;
}

void console_logger_handle_record(LoggerHandler *self, LogRecord **rec) {
    assert(NULL != self);

    printf(
        "[%s]: %s:%d: %s\n",
        log_level_describe((*rec)->level),
        (*rec)->file,
        (*rec)->line,
        (*rec)->message
    );

    if (NULL != (*rec)->additional_data) {
        char *bt = bt_to_string((*rec)->additional_data);
        if (NULL != bt) {
            printf("%s\n", bt);
            free(bt);
        }
    }
}

void console_logger_release(LoggerHandler *self) {
    free(self);
}

void console_handler_set_level(struct ConsoleHandler *self, LogLevel max_level) {
    assert(NULL != self);

    self->max_level = max_level;
}

ConsoleHandler *console_handler_init(LogLevel max_level) {
    ConsoleHandler *self = malloc(sizeof(ConsoleHandler));
    if (NULL != self) {
        self->parent = logger_handler_init(
            console_logger_can_handle,
            console_logger_handle_record,
            console_logger_release
        );
        console_handler_set_level(self, max_level);
    }

    return self;
}

struct FileHandler {
    LoggerHandler parent;
    FILE *fd;
    LogLevel max_level;
};

bool file_logger_can_handle(LoggerHandler *self, LogLevel level) {
    FileHandler *fh = (FileHandler *)self;

    return level >= fh->max_level;
}

void file_logger_handle_record(LoggerHandler *self, LogRecord **rec) {
    FileHandler *fh = (FileHandler *)self;

    if (NULL != fh->fd) {
        int res = fprintf(
            fh->fd,
            "[%s]: %s:%d: %s\n",
            log_level_describe((*rec)->level),
            (*rec)->file,
            (*rec)->line,
            (*rec)->message
        );

        if (res <= 0) {
            printf("Failed to write log\n");
        }

        if (NULL != (*rec)->additional_data) {
            char *bt = bt_to_string((*rec)->additional_data);
            if (NULL != bt) {
                int res = fprintf(fh->fd, "%s\n", bt);
                free(bt);

                if (res <= 0) {
                    printf("Failed to write log\n");
                }
            }
        }
    }
}

void file_logger_release(LoggerHandler *self) {
    FileHandler *fh = (FileHandler *)self;
    if (NULL != fh->fd) {
        fclose(fh->fd);
    }

    free(fh);
}

FileHandler *file_handler_init(LogLevel max_level, char *filename) {
    FileHandler *self = calloc(1, sizeof(FileHandler));
    if (NULL != self) {
        self->parent = logger_handler_init(
            file_logger_can_handle,
            file_logger_handle_record,
            file_logger_release
        );
        file_handler_set_level(self, max_level);
        file_handler_set_file(self, filename);
    }

    return self;
}

void file_handler_set_level(struct FileHandler *self, LogLevel max_level) {
    assert(NULL != self);

    self->max_level = max_level;
}

void file_handler_set_file(struct FileHandler *self, char *filename) {
    assert(NULL != self);

    if (NULL != self->fd) {
        fclose(self->fd);
    }

    FILE *fd = fopen(filename, "w+");
    if (NULL == fd) {
        printf("Couldn't open file `%s`", filename);
        return;
    }

    self->fd = fd;
}

struct SyslogHandler {
    LoggerHandler parent;
    LogLevel max_level;
};

bool syslog_handler_can_handle(LoggerHandler *self, LogLevel level) {
    SyslogHandler *sh = (SyslogHandler *)self;

    return level >= sh->max_level;
}

void syslog_logger_handle_record(LoggerHandler *self, LogRecord **rec) {
    assert(NULL != self);
    assert(NULL != self);

    #define SYSLOG(loglevel) { \
        syslog( \
                loglevel, \
                "[%s]: %s:%d: %s\n", \
                log_level_describe((*rec)->level), \
                (*rec)->file, \
                (*rec)->line, \
                (*rec)->message \
            ); \
    }
    #define SYSLOG_BREAK(loglevel) { SYSLOG(loglevel); break; }
    #define SYSLOG_BT(loglevel) { \
        SYSLOG(loglevel); \
        if (NULL != (*rec)->additional_data) { \
            char *bt = bt_to_string((*rec)->additional_data);\
            if (NULL != bt) {\
                syslog(loglevel, "%s\n", bt); \
                free(bt); \
            } \
        } \
        break; \
    }

    switch ((*rec)->level) {
        case TRACE:
        case DEBUG: SYSLOG_BREAK(LOG_DEBUG);
        case INFO: SYSLOG_BREAK(LOG_INFO);
        case WARN: SYSLOG_BREAK(LOG_WARNING);
        case ERROR: SYSLOG_BT(LOG_ERR);
        case CRIT: SYSLOG_BT(LOG_CRIT);
    }
}

void syslog_handler_release(LoggerHandler *self) {
    closelog();
    free(self);
}

struct SyslogHandler *syslog_handler_init(int options, int facility, LogLevel max_level) {
    SyslogHandler *self = malloc(sizeof(SyslogHandler));
    if (NULL != self) {
        self->max_level = max_level;
        self->parent = logger_handler_init(
            syslog_handler_can_handle,
            syslog_logger_handle_record,
            syslog_handler_release
        );
        openlog(NULL, options, facility); //-V575 Оно так и предусмотренно, чтобы имя программы в лог писалось
    }

    return self;
}
