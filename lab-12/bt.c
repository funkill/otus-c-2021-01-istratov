// This is a personal academic project. Dear PVS-Studio, please check it.

// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "bt.h"

struct Backtrace {
    char **strings;
    int count;
};

Backtrace *bt_from_strings(char **strings, int count) {
    assert(NULL != strings);

    Backtrace *self = malloc(sizeof(Backtrace));
    if (NULL != self) {
        self->strings = strings;
        self->count = count;
    }

    return self;
}

void bt_deinit(Backtrace *self) {
    assert(NULL != self);

    free(self->strings);
    free(self);
}

char *bt_to_string(Backtrace *self) {
    int cap = self->count;
    int len = 0;
    char *bt = calloc(cap, sizeof(char));
    if (NULL == bt) {
        printf("Can't create buffer for backtrace\n");
        return NULL;
    }

    for (int i = 0; i < self->count; i++) {
        int str_len = strlen(self->strings[i]);
        if (cap < len + str_len + 1) {
            int new_cap = (cap + len + str_len + 1) * 2;
            char *new_bt = realloc(bt, new_cap);
            if (NULL == new_bt) {
                printf("Reduce backtrace (new memory not alloc)");
                return bt;
            }

            bt = new_bt;
            cap = new_cap;
        }

        strncat(bt, self->strings[i], cap);
        strncat(bt, "\n", cap);
        len += str_len + 1;
    }

    return bt;
}
