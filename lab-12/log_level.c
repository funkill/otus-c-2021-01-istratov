// This is a personal academic project. Dear PVS-Studio, please check it.

// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include <stdio.h>
#include <stdlib.h>

#include "log_level.h"

#define DESCRIBE(level) case level: return #level; break;

char *log_level_describe(LogLevel level) {
    switch (level) {
        DESCRIBE(TRACE)
        DESCRIBE(DEBUG)
        DESCRIBE(INFO)
        DESCRIBE(WARN)
        DESCRIBE(ERROR)
        DESCRIBE(CRIT)
    }

    printf("Unknown log level `%d`", level);
    exit(1);
}
