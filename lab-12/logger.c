// This is a personal academic project. Dear PVS-Studio, please check it.

// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include <assert.h>
#include <stdlib.h>

#include "logger.h"
#include "linked_list.h"

struct Logger {
    LinkedList *handlers;
};

static Logger *LOGGER;

void deinit_handlers(LoggerHandler *handler) {
    handler->release(handler);
}

struct Logger *logger_init_empty() {
    Logger *self = calloc(1, sizeof(Logger));
    if (NULL == self) {
        return NULL;
    }

    self->handlers = ll_init();
    if (NULL == self->handlers) {
        logger_deinit(self);

        return NULL;
    }

    ll_set_deinit_handler(self->handlers, (void((*)(void *)))deinit_handlers);

    LOGGER = self;

    return self;
}

void logger_push_handler(struct Logger *self, LoggerHandler *handler) {
    ll_push(self->handlers, handler);
}

void logger_log_private(Logger *self, LogLevel level, char *message, char *file, int line, Backtrace *additional_data) {
    LogRecord *record = log_record_init(level, message, file, line, additional_data);
    LoggerHandler **handler;
    for (int i = 0; NULL != (handler = (LoggerHandler **)ll_get(self->handlers, i)); ++i) {
        if ((*handler)->can_handle((*handler), level)) {
            (*handler)->handle_record((*handler), &record);
        }
    }

    log_record_deinit(record);
}

void logger_log_worker(LogLevel level, char *message, char *file, int line, Backtrace *additional_data) {
    logger_log_private(LOGGER, level, message, file, line, additional_data);
}

void logger_deinit(struct Logger *self) {
    ll_deinit(self->handlers);
    self->handlers = NULL;
    free(self);
}

Backtrace *add_backtrace() {
    #define BUFF_SIZE 100
    void *buff[BUFF_SIZE];
    int nptrs = backtrace(buff, BUFF_SIZE);
    char **strings = backtrace_symbols(buff, nptrs);
    if (NULL == strings) {
        perror("Couldn't fetch backtrace symbols");
        exit(1);
    }

    Backtrace *bt = bt_from_strings(strings, nptrs);
    if (NULL == bt) {
        free(strings);
    }

    return bt;
}
