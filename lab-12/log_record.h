#ifndef __LOG_RECORD_H
#define __LOG_RECORD_H

#include "bt.h"
#include "log_level.h"

typedef struct LogRecord {
    LogLevel level;
    char *message;
    char *file;
    int line;
    void *additional_data;
} LogRecord;

LogRecord *log_record_init(LogLevel level, char *message, char *file, int line, Backtrace *additional_data);
void log_record_deinit(LogRecord *self);

#endif // __LOG_RECORD_H