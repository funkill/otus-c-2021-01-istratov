#ifndef __BT_H
#define __BT_H

typedef struct Backtrace Backtrace;

Backtrace *bt_from_strings(char **, int);
void bt_deinit(Backtrace *);
char *bt_to_string(Backtrace *);

#endif // __BT_H