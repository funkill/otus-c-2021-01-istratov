#ifndef __LOGGER_HANDLER_H
#define __LOGGER_HANDLER_H

#include "log_level.h"
#include "log_record.h"

typedef struct LoggerHandler {
    bool((*can_handle)(struct LoggerHandler *, LogLevel));
    void((*handle_record)(struct LoggerHandler *, LogRecord **));
    void((*release)(struct LoggerHandler *));
} LoggerHandler;

typedef struct ConsoleHandler ConsoleHandler;

ConsoleHandler *console_handler_init(LogLevel max_level);
void console_handler_set_level(ConsoleHandler *self, LogLevel max_level);

typedef struct FileHandler FileHandler;
FileHandler *file_handler_init(LogLevel max_level, char *filename);
void file_handler_set_level(FileHandler *self, LogLevel max_level);
void file_handler_set_file(FileHandler *self, char *filename);

typedef struct SyslogHandler SyslogHandler;
SyslogHandler *syslog_handler_init(int options, int facility, LogLevel max_level);

#endif // __LOGGER_HANDLER_H