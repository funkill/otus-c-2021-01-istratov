#ifndef __LOGGER_C
#define __LOGGER_C

#include <stdio.h>
#include <stdbool.h>
#include <execinfo.h>

#include "bt.h"
#include "log_level.h"
#include "logger_handler.h"

typedef struct Logger Logger;

Logger *logger_init_empty();
// disable for simplifier
// Logger *logger_init_with_handlers(LoggerHandler *);
void logger_log_worker(LogLevel level, char *message, char *file, int line, Backtrace *additional_data);
void logger_push_handler(Logger *, LoggerHandler *);

Backtrace *add_backtrace();
// Идея additional_data была в том, что туда можно положить HashMap с произвольными данными,
// необходимыми для отладки. Для error и crit в этот же HashMap добавлялся backtrace
#define logger_log(level, message, additional_data) {                       \
logger_log_worker(level, message, __FILE__, __LINE__, additional_data);     \
}
#define logger_trace(message) { logger_log(TRACE, message, NULL) }
#define logger_debug(message) { logger_log(DEBUG, message, NULL) }
#define logger_info(message) { logger_log(INFO, message, NULL) }
#define logger_warn(message) { logger_log(WARN, message, NULL) }
#define logger_error(message) { logger_log(ERROR, message, add_backtrace()) }
#define logger_crit(message) { logger_log(CRIT, message, add_backtrace()) }
void logger_deinit(Logger *);

#endif
