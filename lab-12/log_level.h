#ifndef __LOG_LEVEL_H
#define __LOG_LEVEL_H

typedef enum {
    TRACE,
    DEBUG,
    INFO,
    WARN,
    ERROR,
    CRIT,
} LogLevel;

char *log_level_describe(LogLevel level);

#endif // __LOG_LEVEL_H