// This is a personal academic project. Dear PVS-Studio, please check it.

// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include "lib.h"

#include <sys/fcntl.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <unistd.h>

#include <string.h>
#include <errno.h>

#include "crc32.h"

#define DEFAULT_CHUNK_SIZE 4096000

result result_ok(uint_least32_t ok) {
    result res = {
        .ok = true,
        .val.ok = ok,
    };

    return res;
}

result result_err(Errors err) {
    result res = {
        .ok = false,
        .val.err = err,
    };

    return res;
}

result checksum(char *file) {
    int fd = open(file, O_RDONLY);
    if (-1 == fd) {
        return result_err(ERR_OPENNING_FILE);
    }

    struct stat st;
    if (-1 == fstat(fd, &st)) {
        close(fd);
        return result_err(ERR_GETTING_STATS);
    }

    crc32_t *hasher = crc32_init();
    if (NULL == hasher) {
        close(fd);
        return result_err(ERR_HASHER_INIT);
    }

    crc32_chunked_t *chunked_hasher = crc32_chunked(hasher);
    if (NULL == chunked_hasher) {
        close(fd);
        crc32_deinit(hasher);

        return result_err(ERR_CHUNKED_INIT);
    }

    size_t len = (size_t)st.st_size;
    if (len <= 0) {
        close(fd);
        crc32_chunked_deinit(chunked_hasher);
        crc32_deinit(hasher);

        return result_err(ERR_ZERO_SIZE);
    }

    size_t offset = 0;
    while (offset < len) {
        void *data = mmap(NULL, DEFAULT_CHUNK_SIZE, PROT_READ, MAP_SHARED, fd, offset);
        if (MAP_FAILED == data) {
            close(fd);
            crc32_chunked_deinit(chunked_hasher);
            crc32_deinit(hasher);

            return result_err(ERR_MMAP);
        }

        size_t rest_size = len - offset;
        crc32_chunked_add_chunk(&chunked_hasher, data, rest_size < DEFAULT_CHUNK_SIZE ? rest_size : DEFAULT_CHUNK_SIZE);
        int res = munmap(data, DEFAULT_CHUNK_SIZE);
        if (res != 0) {
            fprintf(stderr, "WARN: unmmap error: %s\n", strerror(errno));
        }

        offset += DEFAULT_CHUNK_SIZE;
    }


    uint_least32_t result = crc32_chunked_finish(chunked_hasher);

    close(fd);
    crc32_deinit(hasher);

    return result_ok(result);
}
