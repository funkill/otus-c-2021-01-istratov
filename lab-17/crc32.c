// This is a personal academic project. Dear PVS-Studio, please check it.

// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include <stdlib.h>

#include "crc32.h"

#define TABLE_SIZE 256

struct crc32_chunked *crc32_chunked_init(const struct crc32 *crc);

struct crc32 {
    uint_least32_t table[TABLE_SIZE];
};

void init_table(struct crc32 **s) {
    uint_least32_t crc; int i, j;
    for (i = 0; i < TABLE_SIZE; i++) {
        crc = i;
        for (j = 0; j < 8; j++) {
            crc = crc & 1 ? (crc >> 1) ^ 0xEDB88320UL : crc >> 1;
        }

        (*s)->table[i] = crc;
    }
}

struct crc32 *crc32_init() {
    struct crc32 *self = calloc(1, sizeof(struct crc32));
    if (NULL != self) {
        init_table(&self);
    }

    return self;
}

struct crc32_chunked *crc32_chunked(const struct crc32 *self) {
    return crc32_chunked_init(self);
}

void crc32_deinit(struct crc32 *self) {
    free(self);
}

struct crc32_chunked {
    const struct crc32 *crc;
    uint_least32_t intermediate;
};

struct crc32_chunked *crc32_chunked_init(const struct crc32 *crc) {
    struct crc32_chunked *self = calloc(1, sizeof(struct crc32_chunked));
    if (NULL != self) {
        self->crc = crc;
        self->intermediate = 0xFFFFFFFFUL;
    }

    return self;
}

void crc32_chunked_deinit(struct crc32_chunked *self) {
    self->crc = NULL;
    free(self);
}

void crc32_chunked_add_chunk(struct crc32_chunked **self, unsigned char *buf, size_t len) {
    uint_least32_t crc = (*self)->intermediate;
    while (len--) {
        crc = (*self)->crc->table[(crc ^ *buf++) & 0xFF] ^ (crc >> 8);
    }

    (*self)->intermediate = crc;
}

uint_least32_t crc32_chunked_finish(struct crc32_chunked *self) {
    uint_least32_t result = self->intermediate ^ 0xFFFFFFFFUL;
    crc32_chunked_deinit(self);

    return result;
}
