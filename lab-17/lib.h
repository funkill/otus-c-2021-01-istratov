#ifndef __LIB_C
#define __LIB_C

#include <stdbool.h>
#include <stdio.h>
#include <stddef.h>
#include <stdint.h>

typedef enum {
    ERR_OPENNING_FILE = -1,
    ERR_GETTING_STATS = -2,
    ERR_HASHER_INIT = -3,
    ERR_CHUNKED_INIT = -4,
    ERR_ZERO_SIZE = -5,
    ERR_MMAP = -6,
} Errors;

typedef struct result_t {
    bool ok;
    union {
        uint_least32_t ok;
        Errors err;
    } val;
} result;

result result_ok(uint_least32_t ok);
result result_err(Errors err);

result checksum(char *file);

#endif // __LIB_C
