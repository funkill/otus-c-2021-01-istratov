#ifndef __CRC32_H
#define __CRC32_H

#include <stddef.h>
#include <stdint.h>

typedef struct crc32_chunked crc32_chunked_t;

void crc32_chunked_add_chunk(crc32_chunked_t **self, unsigned char *buf, size_t len);
uint_least32_t crc32_chunked_finish(crc32_chunked_t *self);
void crc32_chunked_deinit(crc32_chunked_t *self);

typedef struct crc32 crc32_t;

crc32_t *crc32_init();
uint_least32_t crc32_string(crc32_t *self, char *s);
void crc32_deinit(crc32_t *self);

crc32_chunked_t *crc32_chunked(const struct crc32 *self);

#endif // __CRC32_H
