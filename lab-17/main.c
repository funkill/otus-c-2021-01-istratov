// This is a personal academic project. Dear PVS-Studio, please check it.

// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include <assert.h>
#include <error.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#include "lib.h"

void print_error(short error) {
    switch (error) {
        case ERR_OPENNING_FILE:
            fprintf(stderr, "Не удалось открыть файл: %s", strerror(errno));
            break;
        case ERR_GETTING_STATS:
            fprintf(stderr, "Не удалось получить данные о файле: %s", strerror(errno));
            break;
        case ERR_HASHER_INIT:
            fprintf(stderr, "Не удалось инициализировать хешер");
            break;
        case ERR_CHUNKED_INIT:
            fprintf(stderr, "Не удалось инициализировать составной хешер");
            break;
        case ERR_ZERO_SIZE:
            fprintf(stderr, "Файл нулевого размера");
            break;
        case ERR_MMAP:
            fprintf(stderr, "Ошибка mmap: %s", strerror(errno));
            break;
        default:
            fprintf(stderr, "Неизвестная ошибка %d", error);
            break;
    }
}

int main(int argc, char **argv) {
    if (argc <= 1) {
        fprintf(stderr, "Первый аргумент должен быть путём до файла\n");
        exit(1);
    }

    result res = checksum(argv[1]);
    if (!res.ok) {
        print_error(res.val.err);

        return res.val.err;
    }

    printf("Контрольная сумма: 0x%X\n", res.val.ok);

    return 0;
}
