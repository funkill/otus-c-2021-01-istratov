#ifndef __STRING_H
#define __STRING_H

#include "scalar.h"

typedef struct string_t string;

string *string_with_capacity(unsigned int);
void string_push(string *, scalar);
unsigned int string_len(string *);
char *string_raw(string *);
void string_destroy(string *);

#endif
