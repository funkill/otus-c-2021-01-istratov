// This is a personal academic project. Dear PVS-Studio, please check it.

// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include <assert.h>
#include <stdlib.h>

#include "string.h"

struct string_t {
    unsigned int len;
    unsigned int cap;
    char *chars;
};

struct string_t *string_with_capacity(unsigned int capacity) {
    char *chars = (char *)calloc(capacity, sizeof(char));
    if (NULL == chars) {
        return NULL;
    }

    struct string_t *str = (string *)malloc(sizeof(struct string_t));
    if (NULL != str) {
        str->len = 0;
        str->cap = capacity;
        str->chars = chars;
    }

    return str;
}

void push_char(struct string_t *str, char ch) {
    assert(str->len < str->cap);
    str->chars[str->len] = ch;
    str->len++;
}

void string_push(struct string_t *str, scalar scal) {
    if (scal.len == TWO_BYTE) {
        push_char(str, scal.value >> 8);
    }

    push_char(str, (char)scal.value);
}

unsigned int string_len(string *str) {
    return str->len;
}

char *string_raw(string *str) {
    return str->chars;
}

void string_destroy(string *str) {
    str->len = 0;
    str->cap = 0;
    free(str->chars);
    free(str);
}
