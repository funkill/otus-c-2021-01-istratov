// This is a personal academic project. Dear PVS-Studio, please check it.

// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include <stdio.h>
#include <stdbool.h>
#include <getopt.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h> // include function strcasecmp on linux

#include "args.h"
#include "errors.h"

void print_usage() {
    printf("otus_converter: simple iconv from cp1251, koi8-r or iso-885905 to utf-8\n"
        "Usage: main -i <file> -o <file> -f <code table>\n"
        "Options:\n"
        "  -i <file>            input file\n"
        "  -o <file>            output file\n"
        "  -f <code table>      code table of input file\n"
        "Valid code tables: cp1251, koi8, iso (case insensitive)\n"
        "\n"
        "For bug reporting instructions, please see:\n"
        "<https://gitlab.com/funkill/otus-c-2021-01-istratov/-/issues>.\n"
    );
}

args_t *uninitialized_args() {
    return malloc(sizeof(args_t));
}

bool validate_args(args_t *args) {
    return args->input != NULL
        && args->output != NULL
        && args->from != NULL;
}

args_code_table *parse_code_table(char *value) {
    args_code_table *table = malloc(sizeof(args_code_table));
    if (NULL == table) {
        printf("Couldn't allocate memory for args table\n");
        exit(MEMORY_ALLOCATION_ERROR);
    }

    if (strcasecmp(value, "KOI8") == 0) {
        *table = KOI8;
    } else if (strcasecmp(value, "CP1251") == 0) {
        *table = CP1251;
    } else if (strcasecmp(value, "ISO") == 0) {
        *table = ISO88595;
    }

    return table;
}

// Function always return args_t or terminate program
args_t *parse_args(int argc, char **argv) {
    args_t *args = uninitialized_args();
    if (NULL == args) {
        printf("Failed to allocate arguments");
        exit(MEMORY_ALLOCATION_ERROR);
    }

    int opt;
    while ((opt = getopt(argc, argv, "i:o:f:h")) != -1) {
        switch (opt) {
            case 'i':
                args->input = optarg;
                break;
            case 'o':
                args->output = optarg;
                break;
            case 'f': {
                args->from = parse_code_table(optarg);
                break;
            }
            case 'h':
                print_usage();
                exit(0);
                break;
        }
    }

    if (!validate_args(args)) {
        print_usage();
        exit(INVALID_ARGS);
    }

    return args;
}
