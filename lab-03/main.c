// This is a personal academic project. Dear PVS-Studio, please check it.

// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include <stdio.h>
#include <stdlib.h>

#include "args.h"
#include "errors.h"
#include "converter.h"
#include "string.h"

#define STR_LEN 100

int main(int argc, char **argv) {
    args_t *args = parse_args(argc, argv);

    FILE *input = fopen(args->input, "rb");
    if (NULL == input) {
        printf("Can't open input file\n");
        exit(INPUT_FILE_NOT_ACCESSIBLE);
    }

    FILE *output = fopen(args->output, "w");
    if (NULL == output) {
        printf("Can't open output file\n");
        exit(OUTPUT_FILE_NOT_ACCESSIBLE);
    }

    void ((*fn)(string *, char **, unsigned long));
    switch (*args->from) {
        case KOI8: fn = from_koi8; break;
        case CP1251: fn = from_cp1251; break;
        case ISO88595: fn = from_iso; break;
        default: {
            printf("Invalid encoding\n");
            exit(INVALID_ENCODING);
        }
    }

    char *c = (char *)malloc(sizeof(char) * STR_LEN);
    if (NULL == c) {
        printf("Couldn't allocate memory for buffer\n");
        exit(MEMORY_ALLOCATION_ERROR);
    }

    unsigned long len;
    while ((len = fread(c, sizeof(char), STR_LEN, input))) {
        if (STR_LEN != len && !feof(input)) {
            printf("Read error");
            exit(READ_FILE_ERROR);
        }

        string *accumulator = string_with_capacity(len * 2);
        if (accumulator == NULL) {
            printf("Memory not allocated");
            exit(MEMORY_ALLOCATION_ERROR);
        }

        fn(accumulator, &c, len);

        unsigned int str_len = string_len(accumulator);
        if (str_len != fwrite(string_raw(accumulator), sizeof(unsigned char), str_len, output)) {
            printf("Write error");
            exit(WRITE_FILE_ERROR);
        }

        string_destroy(accumulator);
    }

    fclose(output);
    fclose(input);
    free(args);
    free(c);

    return 0;
}
