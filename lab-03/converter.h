#ifndef __CONVERTER_H
#define __CONVERTER_H

#include "string.h"
#include "scalar.h"

typedef struct {
    unsigned char in;
    scalar utf;
} code_table;

void from_koi8(string *, char **, unsigned long);
void from_cp1251(string *, char **, unsigned long);
void from_iso(string *, char **, unsigned long);

#endif
