#ifndef __SCALAR_H
#define __SCALAR_H

typedef enum {
    ONE_BYTE,
    TWO_BYTE,
} scalar_length;

typedef struct {
    unsigned short value;
    scalar_length len;
} scalar;

#endif
