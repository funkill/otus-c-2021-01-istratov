// This is a personal academic project. Dear PVS-Studio, please check it.

// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include <stdio.h>
#include <stdlib.h>

#include "converter.h"
#include "cp1251.h"
#include "koi8.h"
#include "iso.h"
#include "errors.h"

scalar corresponding_char(unsigned char input, const code_table *ct) {
    if (input < 128) {
        scalar sc = { .value = (unsigned short)input, .len = ONE_BYTE };

        return sc;
    }

    for (int i = 0; i <= 55; ++i) {
        if (ct[i].in == input) {
            return ct[i].utf;
        }
    }

    printf("Failed to lookup char in code table\n");
    exit(INVALID_CHAR);
}

void convert(string *acc, char **input, unsigned long size, const code_table *ct) {
    for (unsigned long i = 0; i < size; ++i) {
        string_push(acc, corresponding_char((*input)[i], ct));
    }
}

void from_koi8(string *acc, char **input, unsigned long size) {
    convert(acc, input, size, &*koi8_table);
}

void from_cp1251(string *acc, char **input, unsigned long size) {
    convert(acc, input, size, &*cp1251_table);
}

void from_iso(string *acc, char **input, unsigned long size) {
    convert(acc, input, size, &*iso_table);
}
