#ifndef __ARGS_H
#define __ARGS_H

typedef enum {
    KOI8 = 1,
    CP1251,
    ISO88595,
} args_code_table;

typedef struct {
    char *input;
    char *output;
    args_code_table *from;
} args_t;

args_t *parse_args(int argc, char **argv);

#endif
