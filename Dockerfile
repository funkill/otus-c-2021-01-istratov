FROM gcc:11.1.0

# INSTALL DEPENDENCIES
RUN apt update -yq \
 && apt install -yq --no-install-recommends wget python3-pip ninja-build valgrind libsqlite3-dev libpq-dev \
 && apt clean -yq

RUN pip3 install meson

# INSTALL PVS-Studio
RUN wget -q -O - https://files.viva64.com/etc/pubkey.txt | apt-key add - \
 && wget -O /etc/apt/sources.list.d/viva64.list \
    https://files.viva64.com/etc/viva64.list \
 && apt update -yq \
 && apt install -yq pvs-studio strace \
 && pvs-studio-analyzer credentials PVS-Studio Free FREE-FREE-FREE-FREE \
 && pvs-studio --version \
 && apt clean -yq

RUN apt update -yq \
 && apt install -yq --no-install-recommends cmake \
   libgstreamer1.0-dev libgstreamer-plugins-base1.0-dev gstreamer1.0-tools gdb \
 && apt clean -yq

WORKDIR /opt/app
VOLUME [ "/opt/app" ]
