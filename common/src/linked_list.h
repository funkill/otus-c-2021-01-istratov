#ifndef __LINKED_LIST_H
#define __LINKED_LIST_H

typedef struct LinkedList LinkedList;

LinkedList *ll_init();
void ll_set_deinit_handler(LinkedList *, void((*)(void *)));
void ll_push(LinkedList *, void *);
void *ll_pop(LinkedList *);
void **ll_get(LinkedList *, int index);
void ll_deinit(LinkedList *);

#endif // __LINKED_LIST_H