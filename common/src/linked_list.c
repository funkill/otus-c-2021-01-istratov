// This is a personal academic project. Dear PVS-Studio, please check it.

// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include <assert.h>
#include <stdlib.h>
#include <stdio.h>

#include "linked_list.h"

typedef struct Node {
    void *val;
    struct Node *next;
} Node;

Node *node_init() {
    return calloc(1, sizeof(Node));
}

Node *node_init_with_data(void *val, Node *next) {
    assert(NULL != val);

    Node *self = node_init();
    if (NULL == self) {
        return NULL;
    }

    self->val = val;
    self->next = next;

    return self;
}

void node_deinit(Node *self) {
    self->val = NULL;
    self->next = NULL;
    free(self);
}

struct LinkedList {
    Node *head;
    Node *tail;
    void((*deinit_handler)(void*));
};

struct LinkedList *ll_init() {
    return calloc(1, sizeof(LinkedList));
}

void ll_push(LinkedList *self, void *data) {
    Node *node = node_init_with_data(data, self->head);

    self->head = node;

    if (NULL == self->tail) {
        self->tail = node;
    }
}

void *ll_pop(LinkedList *self) {
    if (NULL == self->head) {
        return NULL;
    }

    Node *node = self->head;
    if (self->head == self->tail) {
        self->head = self->tail = NULL;
    } else {
        self->head = node->next;
    }

    void *val = node->val;
    node_deinit(node);

    return val;
}

void **ll_get(LinkedList *self, int index) {
    if (NULL == self->head) {
        return NULL;
    }

    int counter = 0;
    Node **n = &(self->head);
    while (counter < index && /*n != self->tail &&*/ NULL != *n) {
        n = &((*n)->next);
        counter++;
    }

    if (NULL == n || NULL == *n) {
        return NULL;
    }

    return &((*n)->val);
}

void ll_set_deinit_handler(LinkedList *self, void((*handler)(void *))) {
    assert(NULL != self);

    self->deinit_handler = handler;
}

void ll_deinit(LinkedList *self) {
    void *item;
    while (NULL != (item = ll_pop(self))) {
        if (NULL != self->deinit_handler) {
            self->deinit_handler(item);
        } else {
            free(item);
        }
    }

    free(self);
}