// This is a personal academic project. Dear PVS-Studio, please check it.

// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include <stdlib.h>
#include <string.h>

#include "strdup.h"

char *_strdup(char *from) {
    char *to;

    size_t len = strlen(from);
    to = calloc(sizeof(char), len + 1);
    if (NULL != to) {
        to = strncpy(to, from, len);
    }

    return to;
}