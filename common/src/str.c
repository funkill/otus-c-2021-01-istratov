// This is a personal academic project. Dear PVS-Studio, please check it.

// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include "str.h"

char *strtolower(char *in) {
    size_t len = strlen(in);
    char *out = (char *)calloc(sizeof(char), len + 1);
    if (NULL != out) {
        for (size_t i = 0; i < len; ++i) {
            out[i] = tolower(in[i]);
        }
    }

    return out;
}
