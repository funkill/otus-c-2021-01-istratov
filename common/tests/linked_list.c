// This is a personal academic project. Dear PVS-Studio, please check it.

// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include <glib-2.0/glib.h>
#include <glib-2.0/glib/gi18n.h>

#include <stdio.h>

#include "linked_list.h"

#define ADD_TEST_NS(namespace, name, fn) {g_test_add_func(namespace name, fn);}
#define ADD_TEST(fn) { ADD_TEST_NS("/linked_list/", #fn, fn) }

#define MALLOC(ty, to, val)                                 \
    ty *to = malloc(sizeof(ty));                            \
    if (NULL == to) {                                       \
        g_test_incomplete("Could not allocate memory");     \
        exit(1); /* for pvs studio */                       \
    }                                                       \
    *to = val;


void test_init_list() {
    g_assert_true(NULL != ll_init());
}

void test_deinit_list() {
    LinkedList *list = ll_init();
    ll_deinit(list);
}

void test_push_data() {
    MALLOC(int, first, 1); //-V522
    MALLOC(int, second, 2);
    MALLOC(int, third, 3);

    LinkedList *list = ll_init();
    ll_push(list, first);
    ll_push(list, second);
    ll_push(list, third);
}

void test_pop_data() {
    MALLOC(int, first, 1);
    MALLOC(int, second, 2);
    MALLOC(int, third, 3);

    LinkedList *list = ll_init();
    ll_push(list, first);
    ll_push(list, second);
    ll_push(list, third);

    g_assert_true(third == (int*)ll_pop(list));
    g_assert_true(second == (int*)ll_pop(list));
    g_assert_true(first == (int*)ll_pop(list));
    g_assert_true(NULL == (int*)ll_pop(list));

    ll_push(list, first);
    g_assert_true(first == (int*)ll_pop(list));
    g_assert_true(NULL == (int*)ll_pop(list));
}

void test_get_by_index() {
    MALLOC(int, first, 1);
    MALLOC(int, second, 2);
    MALLOC(int, third, 3);

    LinkedList *list = ll_init();

    g_assert_true(NULL == ll_get(list, 0));

    ll_push(list, first);
    ll_push(list, second);
    ll_push(list, third);

    g_assert_true(second == *ll_get(list, 1));
    g_assert_true(third == *ll_get(list, 0));
    g_assert_true(first == *ll_get(list, 2));
    g_assert_true(first == *ll_get(list, 2));
    g_assert_true(NULL == ll_get(list, 3));
}

int main(int argc, char **argv) {
    g_test_init (&argc, &argv, NULL);

    ADD_TEST(test_init_list);
    ADD_TEST(test_push_data);
    ADD_TEST(test_pop_data);
    ADD_TEST(test_deinit_list);
    ADD_TEST(test_get_by_index);

    return g_test_run();
}
