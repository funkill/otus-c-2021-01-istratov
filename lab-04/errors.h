#ifndef __ERRORS_H
#define __ERRORS_H

typedef enum {
    INVALID_ARGUMENTS = 1,
    ZIPPED_INITIALIZATION_FAILED,
    FILE_CANNOT_BE_OPENED,
    SEEKING_FAILED,
    GETTING_POSITION_FAILED,
    FAILED_TO_READ,
    ALLOCATION_ERROR,
} errors;

#endif
