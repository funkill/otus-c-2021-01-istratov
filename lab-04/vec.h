#ifndef __VEC_H
#define __VEC_H

#include <stdint.h>

typedef struct vec_t vec;

vec *vec_init();
vec *vec_with_capacity(uint32_t);
void vec_push(vec *, void *);
uint32_t vec_len(vec *);
void *vec_get(vec *, uint32_t);
void vec_deinit(vec *v, void ((*fn)(void *)));

#endif
