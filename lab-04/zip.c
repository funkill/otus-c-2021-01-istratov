// This is a personal academic project. Dear PVS-Studio, please check it.

// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include <stdlib.h>

#include "zip.h"

typedef zip_central_catalog_complete zccc;

zccc *zip_central_catalog_complete_init(
    zip_central_catalog *cc,
    char *filename) {
        zccc *c = (zccc *)malloc(sizeof(zccc));
        if (NULL != c) {
            c->cc = cc;
            c->filename = filename;
        }

        return c;
}

void zip_central_catalog_complete_deinit(zccc *c) {
    free(c->cc);
    free(c->filename);
    free(c);
}
