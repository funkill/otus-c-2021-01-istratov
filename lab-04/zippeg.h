#ifndef __ZIPPEG_H
#define __ZIPPEG_H

#include <stdio.h>

#include "vec.h"

typedef struct zippeg_t zippeg;

typedef enum {
    RARJPEG,
    NOT_RARJPEG,
} rarjpeg_flag;

zippeg *zippeg_init(FILE *);
zippeg *zippeg_open(char *);
rarjpeg_flag zippeg_get_central_catalogs(zippeg *, vec *);
void zippeg_deinit(zippeg *);

#endif
