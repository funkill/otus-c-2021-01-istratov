#ifndef __ZIP_H
#define __ZIP_H

#include <stdint.h>

static const uint32_t zip_eocd_signature = 0x06054b50;
typedef struct {
    // uint32_t signature; // 0x06054b50
    uint16_t disk_number;
    uint16_t central_catalog_disk;
    uint16_t cc_count_on_current_disk;
    uint16_t cc_count;
    uint32_t cc_size;
    uint32_t cc_offset;
    uint16_t comment_len;
} zip_eocd;

static const uint32_t zip_file_header_signature = 0x04034b50;

#pragma pack(push, 1)
typedef struct __attribute__((__packed__)) {
    // uint32_t signature; // 0x04034b50
    uint16_t min_version;
    uint16_t bit_flag;
    uint16_t compression_method;
    uint16_t last_modified_time;
    uint16_t last_modified_date;
    uint32_t crc32;
    uint32_t compressed_size;
    uint32_t uncompressed_size;
    uint16_t filename_len;
    uint16_t addition_space_len;
    // char *filename;
} zip_file_header;
#pragma pack(pop)

static const uint32_t zip_central_catalog_signature = 0x02014b50;

#pragma pack(push, 1)
typedef struct __attribute__((__packed__)) {
    // uint32_t signature; // 0x02014b50
    uint16_t version;
    uint16_t min_version;
    uint16_t bit_flag;
    uint16_t compression_method;
    uint16_t last_modified_time;
    uint16_t last_modified_date;
    uint32_t crc32;
    uint32_t compressed_size;
    uint32_t uncompressed_size;
    uint16_t filename_len;
    uint16_t addition_space_len;
    uint16_t comment_len;
    uint16_t disk_number;
    uint16_t attributes_internal;
    uint32_t attributes_external;
    uint32_t header_offset;
    // char *filename;
} zip_central_catalog;
#pragma pack(pop)

typedef struct {
    zip_central_catalog *cc;
    char *filename;
} zip_central_catalog_complete;

zip_central_catalog_complete *zip_central_catalog_complete_init(
    zip_central_catalog *cc,
    char *filename);
void zip_central_catalog_complete_deinit(zip_central_catalog_complete *c);

#endif
