// This is a personal academic project. Dear PVS-Studio, please check it.

// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

#include "errors.h"
#include "vec.h"
#include "zippeg.h"
#include "zip.h"

struct zippeg_t {
    FILE *fd;
};

// How i can sad what i consume file descriptor? I need move semantic
zippeg *zippeg_init(FILE *fd) {
    assert(NULL != fd);

    zippeg *zp = (zippeg *)malloc(sizeof(zippeg));
    if (NULL != zp) {
        zp->fd = fd;
    }

    return zp;
}

zippeg *zippeg_open(char *path) {
    FILE *fd = fopen(path, "rb");
    if (NULL == fd) {
        printf("File `%s` not found or cannot be openned\n", path);
        exit(FILE_CANNOT_BE_OPENED);
    }

    return zippeg_init(fd);
}

void fseek_or_fail(FILE *fd, long pos, int origin) {
    assert(NULL != fd);

    if (fseek(fd, pos, origin) != 0) {
        printf("Seeking failed\n");
        exit(SEEKING_FAILED);
    }
}

long zippeg_find_eocd(FILE *fd, zip_eocd *eocd) {
    assert(NULL != fd);

    unsigned long signature_size = sizeof(zip_eocd_signature);
    long pos = (sizeof(zip_eocd) - signature_size) * -1L; // take neg variant for prevent
                                                          // negativing position in cycle
    uint32_t signature;

    fseek_or_fail(fd, 0L, SEEK_END);
    long filesize = ftell(fd) * -1;

    while (!eocd || pos != filesize) { // don't check feof because we walk from end to start
        fseek_or_fail(fd, pos, SEEK_END);

        if (1 != fread(&signature, signature_size, 1, fd) && ferror(fd)) {
            printf("Failed to read eocd signature\n");
            exit(FAILED_TO_READ);
        }

        if (zip_eocd_signature != signature) {
            pos -= 1;
            continue;
        }

        // fseek_or_fail(fd, signature_size * -1, SEEK_CUR);
        unsigned long eocd_size = sizeof(zip_eocd);
        if (1 != fread(eocd, eocd_size, 1, fd) && ferror(fd)) {
            printf("Reading failed eocd\n");
            exit(FAILED_TO_READ);
        }

        break;
    }

    return pos == filesize
        ? -1
        : pos * -1;
}

void get_central_catalogs(FILE *fd, vec *catalogs) {
    assert(NULL != fd);
    assert(NULL != catalogs);

    unsigned long signature_size = sizeof(zip_central_catalog_signature);
    uint32_t signature;
    unsigned long catalog_size = sizeof(zip_central_catalog);

    long pos = 0;
    while (!feof(fd)) {
        fseek_or_fail(fd, pos, SEEK_CUR);
        if (1 != fread(&signature, signature_size, 1, fd) && ferror(fd)) {
            printf("Reading catalog signature failed\n");
            exit(FAILED_TO_READ);
        }

        if (zip_central_catalog_signature != signature) {
            pos = 1;
            continue;
        }

        // fseek_or_fail(fd, signature_size * -1, SEEK_CUR);
        zip_central_catalog *catalog = (zip_central_catalog *)malloc(catalog_size);
        if (NULL == catalog) {
            printf("Can not allocate memory for catalog\n");
            exit(ALLOCATION_ERROR);
        }

        if (1 != fread(catalog, catalog_size, 1, fd) && ferror(fd)) {
            printf("Readering catalog error\n");
            exit(FAILED_TO_READ);
        }

        char *filename = (char *)calloc(catalog->filename_len + 1, sizeof(char));
        if (NULL == filename) {
            printf("Can not allocate memory for filename\n");
            exit(ALLOCATION_ERROR);
        }

        if (1 != fread(filename, catalog->filename_len, 1, fd) && ferror(fd)) {
            printf("Readering filename error\n");
            exit(FAILED_TO_READ);
        }
        filename[catalog->filename_len] = '\0';
        fseek_or_fail(fd, catalog->comment_len + catalog->addition_space_len, SEEK_CUR);

        zip_central_catalog_complete *complete_catalog =
            zip_central_catalog_complete_init(catalog, filename);
        vec_push(catalogs, complete_catalog);
    }
}

rarjpeg_flag zippeg_get_central_catalogs(zippeg *zp, vec *catalogs) {
    zip_eocd *eocd = (zip_eocd *)malloc(sizeof(zip_eocd));
    if (NULL == eocd) {
        printf("Can not allocate memory for eocd\n");
        exit(ALLOCATION_ERROR);
    }

    int eocd_position = zippeg_find_eocd(zp->fd, eocd);
    if (eocd_position < 0) {
        return NOT_RARJPEG;
    }

    uint32_t zip_start = eocd_position + eocd->cc_size;
    fseek_or_fail(zp->fd, zip_start * -1L, SEEK_END);
    get_central_catalogs(zp->fd, catalogs);

    free(eocd);

    return RARJPEG;
}

void zippeg_deinit(zippeg *zp) {
    fclose(zp->fd);
    free(zp);
}
