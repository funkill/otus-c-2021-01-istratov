// This is a personal academic project. Dear PVS-Studio, please check it.

// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include <stdio.h>
#include <stdlib.h>

#include "errors.h"
#include "vec.h"
#include "zip.h"
#include "zippeg.h"

int main(int argc, char **argv) {
    if (argc < 2) {
        printf("You must set file as argument of program\n");
        exit(INVALID_ARGUMENTS);
    }

    zippeg *zp = zippeg_open(argv[1]);
    if (NULL == zp) {
        printf("Failed initialize zippeg");
        exit(ZIPPED_INITIALIZATION_FAILED);
    }

    vec *catalogs = vec_init();
    rarjpeg_flag flag = zippeg_get_central_catalogs(zp, catalogs);
    if (NOT_RARJPEG == flag) {
        printf("File not Rarjpeg\n");
        return 0;
    }

    printf("File is Rarjpeg\nContent:\n");
    for (uint32_t i = 0; i < vec_len(catalogs); ++i) {
        zip_central_catalog_complete *catalog = (zip_central_catalog_complete *)vec_get(catalogs, i);
        printf("\t%s\n", catalog->filename);
    }

    vec_deinit(catalogs, (void ((*)(void *)))zip_central_catalog_complete_deinit);

    zippeg_deinit(zp);

    return 0;
}
