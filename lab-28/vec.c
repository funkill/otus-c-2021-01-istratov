// This is a personal academic project. Dear PVS-Studio, please check it.

// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "vec.h"

static const size_t ptr_size = sizeof(void *);

struct vec_t {
    uint32_t capacity;
    uint32_t len;
    void **data;
};

vec *vec_with_capacity(uint32_t cap) {
    void *data = calloc(cap, ptr_size);
    vec *vector = (vec *)malloc(sizeof(vec));
    if (NULL != vector) {
        vector->data = data;
        vector->capacity = cap;
        vector->len = 0;
    }

    return vector;
}

vec *vec_init() {
    return vec_with_capacity(1);
}

void vec_deinit(vec *v, void ((*fn)(void *))) {
    for (uint32_t i = 0; i < v->len; ++i) {
        fn(v->data[i]);
    }

    free(v->data);
    free(v);
}

void vec_grow(vec *v) {
    if (NULL == v || NULL == v->data) {
        printf("Growing null or empty vector\n");
        exit(1);
    }

    uint32_t new_cap = v->capacity * 2;
    void **new_data = (void **)calloc(new_cap, ptr_size);
    if (NULL == new_data) {
        printf("Couldn't allocate memory for growing vector\n");
        exit(1);
    }

    unsigned long item_size = sizeof(void*);

    memcpy(new_data, v->data, v->capacity * item_size);
    void *old_data = v->data;
    v->data = new_data;
    v->capacity = new_cap;
    free(old_data);
}

void vec_push(vec *v, void *data) {
    assert(NULL != v);

    if (v->len == v->capacity) {
        vec_grow(v);
    }

    v->data[v->len] = data;
    v->len++;
}

uint32_t vec_len(vec *v) {
    assert(NULL != v);

    return v->len;
}

void *vec_get(vec *v, uint32_t index) {
    assert(NULL != v);

    if (v->len < index) {
        return NULL;
    }

    return v->data[index];
}
