// This is a personal academic project. Dear PVS-Studio, please check it.

// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include <assert.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "oscar.h"
#include "strdup.h"
#include "vec.h"

struct oscar {
    vec *names;
    vec *values;
};

oscar_t *oscar_init() {
    oscar_t *self = malloc(sizeof(oscar_t));
    if (NULL != self) {
        self->names = vec_init();
        self->values = vec_init();
    }

    return self;
}

void oscar_deinit(oscar_t *self) {
    vec_deinit(self->names, free);
    vec_deinit(self->values, free);

    free(self);
}

void oscar_add_string(oscar_t *self, char *name, char *value) {
    assert(vec_len(self->names) == vec_len(self->values));

    vec_push(self->names, name);
    vec_push(self->values, value);
}

void oscar_add_int(oscar_t *self, char *name, int value) {
    assert(vec_len(self->names) == vec_len(self->values));

    int *val = calloc(1, sizeof(int));
    if (!val) {
        printf("Couldn't alloc mem for int val\n");
        return;
    }

    *val = value;

    vec_push(self->names, name);
    vec_push(self->values, val);
}

int oscar_get_index(vec *self, char *name) {
    assert(NULL != self);

    uint32_t len = vec_len(self);
    if (0 == len) {
        return -1;
    }

    size_t name_len = strlen(name);
    for (uint32_t i = 0; i < len; ++i) {
        char *n = vec_get(self, i);
        if (0 == strncmp(name, n, name_len)) {
            return i;
        }
    }

    return -1;
}

int oscar_get_int(oscar_t *self, char *name) {
    assert(vec_len(self->names) == vec_len(self->values));

    int *value = (int *)oscar_get_string(self, name);
    if (NULL == value) {
        return -1;
    }

    return *value;
}

char *oscar_get_string(oscar_t *self, char *name) {
    assert(vec_len(self->names) == vec_len(self->values));

    int idx = oscar_get_index(self->names, name);
    if (-1 == idx) {
        return NULL;
    }

    return vec_get(self->values, idx);
}

void oscar_debug(oscar_t *self) {
    printf("Oscar");
    int len = vec_len(self->names);
    if (len == 0) {
        printf(" {}\n");
        return;
    }

    printf(" { ");
    for (int i = 0; i < len; ++i) {
        printf("%s: %s", (char*)vec_get(self->names, i), (char*)vec_get(self->values, i));
        if (i != len - 1) {
            printf(", ");
        }
    }
    printf(" }\n");
}