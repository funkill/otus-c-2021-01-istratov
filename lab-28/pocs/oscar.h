#ifndef __OSCAR_H
#define __OSCAR_H

typedef struct oscar oscar_t;

oscar_t *oscar_init();
void oscar_deinit(oscar_t *self);

void oscar_add_string(oscar_t *self, char *name, char *value);
void oscar_add_int(oscar_t *self, char *name, int value);

int oscar_get_int(oscar_t *self, char *name);
char *oscar_get_string(oscar_t *self, char *name);

#define oscar_add(self, name, x) _Generic((x), \
    char *: oscar_add_string, \
    const char *: oscar_add_string, \
    const unsigned char *: oscar_add_string, \
    int: oscar_add_int \
)(self, name, x)

void oscar_debug(oscar_t *self);

#endif // __OSCAR_H