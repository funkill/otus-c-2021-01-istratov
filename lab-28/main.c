// This is a personal academic project. Dear PVS-Studio, please check it.

// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include <stdio.h>
#include <stdlib.h>

#include "strdup.h"
#include "vec.h"
#include "connectors/connector.h"
#include "pocs/oscar.h"

int main(int argc, char **argv) {
    if (argc < 4) {
        printf("Invalid arguments!\n");
        return 1;
    }

    char *conn_str = argv[1];
    if (!conn_str) {
        printf(
            "First argument must be valid connection string!\n"
            "Supported SQLite and PostgreSQL\n"
            "Examples:\n"
            "SQLite: sqlite:///path/to/db.sqlite\n"
            "PostgreSQL: postgresql://login:password@host:port/db_name?params\n"
        );
        return 1;
    }

    connector_t *conn = connector_connect(conn_str);
    if (NULL == conn) {
        printf("Error on connecting to db\n");
        return 1;
    }

    char *table = argv[2];
    if (!table) {
        printf("Second param must be table name\n");
        connector_close(conn);
        return 1;
    }

    char *field = argv[3];
    if (!field) {
        printf("Third param must be field name\n");
        connector_close(conn);
        return 1;
    }

    int sum = connector_sum(conn, table, field);
    printf("sum: %d\n", sum);

    int min = connector_min(conn, table, field);
    printf("min: %d\n", min);

    int max = connector_max(conn, table, field);
    printf("max: %d\n", max);

    float avg = connector_avg(conn, table, field);
    printf("avg: %f\n", avg);

    double var = connector_var(conn, table, field);
    printf("variance: %f\n", var);

    connector_close(conn);

    return 0;
}
