// This is a personal academic project. Dear PVS-Studio, please check it.

// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include <assert.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <libpq-fe.h>

#include "connector.h"
#include "connector+private.h"
#include "pgsql_connector.h"
#include "vec.h"
#include "strdup.h"
#include "../pocs/oscar.h"

struct pgsql_connector {
    struct connector parent;
    PGconn *db;
};

void pgsql_connector_destroy(connector_t *self) {
    pgsql_connector_t *this = (pgsql_connector_t *)self;
    if (NULL != this->db) {
        PQfinish(this->db);
        this->db = NULL;
    }
    free(this);
}

int pgsql_connector_init(connector_t *self, char *dsn) {
    assert(NULL != self);
    assert(NULL != dsn);

    pgsql_connector_t *this = (pgsql_connector_t *)self;

    this->db = PQconnectdb(dsn);
    if (CONNECTION_OK != PQstatus(this->db)) {
        char *msg = PQerrorMessage(this->db);
        printf("PGSQL error: %s\n", msg);
        PQfinish(this->db);
        this->db = NULL;
        return 1;
    }

    return 0;
}

vec *pgsql_connector_fetch_all(connector_t *this, char *table) {
    #define QUERY "SELECT * FROM "
    #define STMT_NAME ""

    pgsql_connector_t *self = (pgsql_connector_t *)this;

    char *query = calloc(strlen(table) + strlen(QUERY) + 1, sizeof(char*));
    if (NULL == query) {
        printf("ERR: failed allocate memory for query\n");
        return NULL;
    }

    int qres = sprintf(query, QUERY " %s", table);
    if (-1 == qres) {
        free(query);
        printf("Error: failed creating query\n");

        return NULL;
    }

    PGresult *res = PQprepare(
        self->db,
        STMT_NAME,
        query,
        0,
        NULL
    );

    free(query);

    if (PGRES_COMMAND_OK != PQresultStatus(res)) {
        printf("PGSQL error: %s\n", PQerrorMessage(self->db));
        PQclear(res);

        return NULL;
    }

    PQclear(res);

    res = PQexecPrepared(self->db, STMT_NAME, 0, NULL, NULL, NULL, 0);
    if (PGRES_TUPLES_OK != PQresultStatus(res)) {
        printf("PGSQL error: %s\n", PQerrorMessage(self->db));
        PQclear(res);

        return NULL;
    }

    vec *v = vec_init();
    if (NULL == v) {
        printf("Couldn't allocate vec\n");

        return NULL;
    }

    int rows_count = PQntuples(res);
    int columns_count = PQnfields(res);
    for (int row = 0; row < rows_count; ++row) {
        oscar_t *item = oscar_init();
        for (int col = 0; col < columns_count; ++col) {
            char *name = _strdup(PQfname(res, col)); //-V773
            char *val = PQgetvalue(res, row, col);
            switch (PQftype(res, col)) {
                case 23: // Int
                    oscar_add(item, name, atoi(val));
                    break;
                case 25: // varchar
                    oscar_add(item, name, _strdup(val));
                    break;
                default:
                    printf("Err: unknown pg type!");
                    continue;
            }
        } // -V773 name dropped on dealloc array

        vec_push(v, item);
    }

    PQclear(res);

    return v;
}

pgsql_connector_t *pgsql_connector_new() {
    pgsql_connector_t *self = calloc(1, sizeof(pgsql_connector_t));
    if (NULL != self) {
        connector_t parent = {
            .conn_init = pgsql_connector_init,
            .conn_destroy = pgsql_connector_destroy,
            .conn_fetch_all = pgsql_connector_fetch_all,
        };
        self->parent = parent;
        self->db = NULL;
    }

    return self;
}
