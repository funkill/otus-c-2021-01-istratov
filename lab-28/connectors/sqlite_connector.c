// This is a personal academic project. Dear PVS-Studio, please check it.

// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include <assert.h>
#include <stdlib.h>
#include <sqlite3.h>
#include <stdio.h>
#include <string.h>

#include "connector.h"
#include "connector+private.h"
#include "sqlite_connector.h"
#include "vec.h"
#include "strdup.h"
#include "../pocs/oscar.h"

struct sqlite_connector {
    struct connector parent;
    sqlite3 *db;
};

void sqlite_connector_destroy(connector_t *self) {
    sqlite_connector_t *this = (sqlite_connector_t *)self;
    sqlite3_close(this->db);
    this->db = NULL;
    free(this);
}

int sqlite_connector_init(connector_t *self, char *filename) {
    assert(NULL != self);
    assert(NULL != filename);

    sqlite_connector_t *this = (sqlite_connector_t *)self;

    int res = sqlite3_open(filename, &this->db);
    if (0 != res) {
        printf("Can't open db: %s\n", sqlite3_errmsg(this->db));
        sqlite3_close(this->db);
        return 1;
    }

    return 0;
}

vec *sqlite_connector_fetch_all(connector_t *this, char *table) {
    #define QUERY "SELECT * FROM "

    sqlite_connector_t *self = (sqlite_connector_t *)this;

    char *query = calloc(strlen(table) + strlen(QUERY) + 1, sizeof(char*));
    if (NULL == query) {
        printf("ERR: failed allocate memory for query\n");
        return NULL;
    }

    int qres = sprintf(query, QUERY " %s", table);
    if (-1 == qres) {
        free(query);
        printf("Error: failed creating query\n");

        return NULL;
    }

    sqlite3_stmt *stmt;
    int res = sqlite3_prepare_v2(
        self->db,
        query,
        -1,
        &stmt,
        0
    );

    free(query);

    if (SQLITE_OK != res) {
        printf("SQLite error: %s\n", sqlite3_errstr(res));

        return NULL;
    }

    vec *v = vec_init();
    if (NULL == v) {
        printf("Couldn't allocate vec\n");

        return NULL;
    }

    int ccount = sqlite3_column_count(stmt);
    while (SQLITE_ROW == (res = sqlite3_step(stmt))) {
        oscar_t *item = oscar_init();
        for (int i = 0; i < ccount; ++i) {
            char *name = _strdup((char *)sqlite3_column_name(stmt, i));
            switch (sqlite3_column_type(stmt, i)) {
                case SQLITE_INTEGER:
                    oscar_add(item, name, sqlite3_column_int(stmt, i));
                    break;
                case SQLITE_TEXT:
                    oscar_add(item, name, _strdup((char*)sqlite3_column_text(stmt, i)));
                    break;
                default:
                    printf("DEBUG: Unimplemented\n");
                    break;
            }
        } // -V773 name dropped on dealloc array

        vec_push(v, item);
    }

    sqlite3_finalize(stmt);

    if (SQLITE_DONE != res) {
        printf("SQLite error: %s\n", sqlite3_errstr(res));
        return NULL;
    }

    return v;
}

sqlite_connector_t *sqlite_connector_new() {
    sqlite_connector_t *self = calloc(1, sizeof(sqlite_connector_t));
    if (NULL != self) {
        connector_t parent = {
            .conn_init = sqlite_connector_init,
            .conn_destroy = sqlite_connector_destroy,
            .conn_fetch_all = sqlite_connector_fetch_all,
        };
        self->parent = parent;
        self->db = NULL;
    }

    return self;
}
