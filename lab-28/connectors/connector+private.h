#ifndef __CONNECTOR_PRIVATE_H
#define __CONNECTOR_PRIVATE_H

struct connector {
    int(*conn_init)(struct connector *self, char *dsn);
    void(*conn_destroy)(struct connector *self);

    vec *(*conn_fetch_all)(struct connector *self, char *table);

    // float(*conn_avg)(struct connector *self);
    // float(*conn_min)(struct connector *self);
    // float(*conn_max)(struct connector *self);
    // float(*conn_sum)(struct connector *self);
};

#endif // __CONNECTOR_PRIVATE_H