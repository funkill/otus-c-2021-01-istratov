#ifndef __PGSQL_CONNECTOR_H
#define __PGSQL_CONNECTOR_H

#include "connector.h"

typedef struct pgsql_connector pgsql_connector_t;

pgsql_connector_t *pgsql_connector_new();
void pgsql_connector_destroy(connector_t *self);

#endif // __PGSQL_CONNECTOR_H