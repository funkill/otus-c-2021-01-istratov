#ifndef __SQLITE_CONNECTOR_H
#define __SQLITE_CONNECTOR_H

#include "connector.h"

typedef struct sqlite_connector sqlite_connector_t;

sqlite_connector_t *sqlite_connector_new();
void sqlite_connector_destroy(connector_t *self);

#endif // __SQLITE_CONNECTOR_H