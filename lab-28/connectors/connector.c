// This is a personal academic project. Dear PVS-Studio, please check it.

// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "connector.h"
#include "connector+private.h"
#include "sqlite_connector.h"
#include "pgsql_connector.h"
#include "str.h"
#include "strdup.h"
#include "vec.h"
#include "../pocs/oscar.h"

#define RUN(action) {                                               \
    vec *all = connector_fetch_all(self, table);                    \
    if (NULL == all) {                                              \
        return -1;                                                  \
    }                                                               \
                                                                    \
    uint32_t len = vec_len(all);                                    \
    for (uint32_t i = 0; i < len; ++i) {                            \
        oscar_t *item = vec_get(all, i);                            \
        int val = oscar_get_int(item, field);                       \
        if (-1 == val) {                                            \
            printf("WARN: wrong field `%s`. Breaking\n", field);    \
            break;                                                  \
        }                                                           \
                                                                    \
        action;                                                     \
    }                                                               \
    vec_deinit(all, (void(*)(void*))oscar_deinit);                  \
}

struct connector *connector_connect(char *conn_str) {
    char *dsn = _strdup(conn_str);
    char *engine = strtok(dsn, ":");
    char *tail = strtok(NULL, ":");

    char *engine_lc = strtolower(engine);
    if (NULL == engine_lc) {
        free(dsn);

        return NULL;
    }

    char *conn_info;
    connector_t *self = NULL;
    if (0 == strncmp("postgresql", engine_lc, strlen("postgresql"))) {
        conn_info = conn_str;
        self = (connector_t *)pgsql_connector_new();
    } else if (0 == strncmp("mysql", engine_lc, strlen("mysql"))) {

    } else if (0 == strncmp("sqlite", engine_lc, strlen("sqlite"))) {
        conn_info = tail;
        self = (connector_t *)sqlite_connector_new();
    } else {
        printf("Unknown engine\n");
        free(dsn);
        free(engine_lc);
        exit(1);
    }

    if (NULL != self) {
        if (0 != self->conn_init(self, conn_info)) {
            free(engine_lc);
            free(dsn);
            connector_close(self);

            return NULL;
        }
    }

    free(engine_lc);
    free(dsn);

    return self;
}

void connector_close(connector_t *self) {
    self->conn_destroy(self);
}

float connector_avg(connector_t *self, char *table, char *field) {
    int l = 0;
    int sum = 0;
    RUN(sum += val; ++l;);

    return (float)sum / (float)l;
}

int connector_max(connector_t *self, char *table, char *field) {
    int max = -1;
    RUN(max = max < val ? val : max);

    return max;
}

int connector_min(connector_t *self, char *table, char *field) {
    int min = INT32_MAX;
    RUN(min = min > val ? val : min);

    return min;
}

int connector_sum(connector_t *self, char *table, char *field) {
    int sum = 0;
    RUN(sum += val);

    return sum;
}

double connector_var(connector_t *self, char *table, char *field) {
    vec *all = connector_fetch_all(self, table);
    if (NULL == all) {
        return -1;
    }

    int sum = 0;
    uint32_t len = vec_len(all);
    for (uint32_t i = 0; i < len; ++i) {
        oscar_t *item = vec_get(all, i);
        int val = oscar_get_int(item, field);
        if (-1 == val) {
            printf("WARN: wrong field `%s`. Breaking\n", field);
            break;
        }

        sum += val;
    }

    float avg = (float)sum / (float)len;

    double sum_of_squares = 0;
    for (uint32_t i = 0; i < len; ++i) {
        oscar_t *item = vec_get(all, i);
        int val = oscar_get_int(item, field);
        if (-1 == val) {
            printf("WARN: wrong field `%s`. Breaking\n", field);
            break;
        }

        sum_of_squares += pow((double)(val - avg), (double)2.0);
    }

    vec_deinit(all, (void(*)(void*))oscar_deinit);

    return sum_of_squares / len;
}

vec *connector_fetch_all(connector_t *self, char *table) {
    return self->conn_fetch_all(self, table);
}
