#ifndef __CONNECTOR_H
#define __CONNECTOR_H

#include "vec.h"

typedef struct oscar oscar_t;

typedef struct connector connector_t;

connector_t *connector_connect(char *dsn);
void connector_close(connector_t *self);

float connector_avg(connector_t *self, char *table, char *field);
int connector_max(connector_t *self, char *table, char *field);
int connector_min(connector_t *self, char *table, char *field);
int connector_sum(connector_t *self, char *table, char *field);
double connector_var(connector_t *self, char *table, char *field);
vec *connector_fetch_all(connector_t *self, char *table);

#endif // __CONNECTOR_H