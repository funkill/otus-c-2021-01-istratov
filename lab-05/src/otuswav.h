#ifndef __OTUSWAV_H
#define __OTUSWAV_H

#include <gst/gst.h>
#include <gst/base/gstpushsrc.h>

G_BEGIN_DECLS

#define _OTUSWAV_TYPE _otuswav_get_type ()
#define _OTUSWAV(obj) (G_TYPE_CHECK_INSTANCE_CAST((obj), _OTUSWAV_TYPE, OtusWav))
#define _OTUSWAV_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST((klass), _OTUSWAV_TYPE, OtusWavClass))
#define _IS_OTUSWAV(obj) (G_TYPE_CHECK_INSTANCE_TYPE((obj), _OTUSWAV_TYPE))
#define _IS_OTUSWAV_CLASS(obj) (G_TYPE_CHECK_CLASS_TYPE((klass), _OTUSWAV_TYPE))

G_DECLARE_FINAL_TYPE (OtusWav, gst_otus_wav_plugin, GST, OTUS_WAV_PLUGIN, GstPushSrc)

typedef struct _OtusWav OtusWav;

GType _otuswav_get_type(void);

G_END_DECLS

#endif
