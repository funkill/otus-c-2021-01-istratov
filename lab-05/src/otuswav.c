// This is a personal academic project. Dear PVS-Studio, please check it.

// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include <stdio.h>
#include <glib/gstdio.h>

#include "otuswav.h"
#include "wav.h"

struct _OtusWav {
    GstPushSrc base_source;
    gchar *location;
    FILE *fd;
    gsize offset;
    gsize size;
};

GST_DEBUG_CATEGORY_STATIC(_otuswav_debug_category);
#define GST_CAT_DEFAULT _otuswav_debug_category

static void _otuswav_set_property(GObject *obj, guint property_id,
                                              const GValue *value, GParamSpec *pspec);
static void _otuswav_get_property(GObject *obj, guint propery_id,
                                              GValue *value, GParamSpec *pspec);

static void _otuswav_finalize(GObject *plugin);

static gboolean _otuswav_start(GstBaseSrc *src);
static gboolean _otuswav_stop(GstBaseSrc *src);
// static GstFlowReturn _otuswav_create(GstPushSrc *src, GstBuffer **buf);
// static gboolean _otuswav_negotiate(GstBaseSrc *src);
static GstFlowReturn _otuswav_fill(GstPushSrc *src, GstBuffer *buf);
static gboolean _otuswav_get_size(GstBaseSrc *src, guint64 *size);

enum
{
    PROP_0,
    PROP_LOCATION
};

/* pad templates */

static GstStaticPadTemplate _otuswav_src_template = GST_STATIC_PAD_TEMPLATE(
    "src", GST_PAD_SRC, GST_PAD_ALWAYS, GST_STATIC_CAPS("audio/x-raw;audio/x-wav;audio/x-rf64"));


G_DEFINE_TYPE_WITH_CODE(
    OtusWav, _otuswav, GST_TYPE_PUSH_SRC,
    GST_DEBUG_CATEGORY_INIT(_otuswav_debug_category, "otuswav", 0,
                            "debug category for otus wav plugin"))

static void _otuswav_class_init(OtusWavClass *klass) {
    GObjectClass *gobject_class = G_OBJECT_CLASS(klass);
    GstPushSrcClass *push_src_class = GST_PUSH_SRC_CLASS(klass);
    GstBaseSrcClass *base_src_class = GST_BASE_SRC_CLASS(klass);

    gst_element_class_add_static_pad_template(GST_ELEMENT_CLASS(klass),
                                              &_otuswav_src_template);

    gobject_class->set_property = _otuswav_set_property;
    gobject_class->get_property = _otuswav_get_property;
    gobject_class->finalize = GST_DEBUG_FUNCPTR(_otuswav_finalize);

    // base_src_class->negotiate = GST_DEBUG_FUNCPTR(_otuswav_negotiate);
    base_src_class->start = GST_DEBUG_FUNCPTR(_otuswav_start);
    base_src_class->stop = GST_DEBUG_FUNCPTR(_otuswav_stop);
    base_src_class->get_size = GST_DEBUG_FUNCPTR(_otuswav_get_size);
    // push_src_class->query = GST_DEBUG_FUNCPTR(_otuswav_query);
    // push_src_class->create = GST_DEBUG_FUNCPTR(_otuswav_create);
    push_src_class->fill = GST_DEBUG_FUNCPTR(_otuswav_fill);

    g_object_class_install_property(
        gobject_class, PROP_LOCATION,
        g_param_spec_string("location", "location", "Path of the audio", "",
                            G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

    gst_element_class_set_static_metadata ((GstElementClass *)push_src_class, "WAV audio demuxer",
        "Codec/Demuxer/Audio",
        "Parse a .wav file into raw audio",
        "Dmitrii Istratov <funkill2@gmail.com>");

}

static void _otuswav_init(OtusWav *plugin) {
    GST_DEBUG_OBJECT(plugin, "Initialize");
    plugin->fd = NULL;
    plugin->offset = 0;
    plugin->location = NULL;
}

static void _otuswav_finalize(GObject *object) {
    GST_DEBUG_OBJECT(object, "Finalize");

    OtusWav *plugin = _OTUSWAV(object);

    plugin->fd = NULL;
    plugin->location = NULL;
    plugin->offset = 0;

    G_OBJECT_CLASS(_otuswav_parent_class)->finalize(object);
}

void _otuswav_set_property(GObject *object, guint property_id,
                                       const GValue *value, GParamSpec *pspec) {
    OtusWav *plugin = _OTUSWAV(object);

    GST_DEBUG_OBJECT(plugin, "set_propery");

    switch (property_id) {
        case PROP_LOCATION:
            plugin->location = g_strdup(g_value_get_string(value));
            break;
        default:
            G_OBJECT_WARN_INVALID_PROPERTY_ID(object, property_id, pspec);
            break;
    }

}

void _otuswav_get_property(GObject *object, guint property_id,
                               GValue *value, GParamSpec *pspec) {
    OtusWav *plugin = _OTUSWAV(object);

    GST_DEBUG_OBJECT(plugin, "get_propery");

    switch (property_id) {
        case PROP_LOCATION:
            g_value_set_string(value, plugin->location);
            break;
        default:
            G_OBJECT_WARN_INVALID_PROPERTY_ID(object, property_id, pspec);
            break;
    }
}

static gboolean _otuswav_get_size(GstBaseSrc *src, guint64 *size) {
    OtusWav *plugin = _OTUSWAV(src);

    GST_WARNING_OBJECT(plugin, "get_size");

    *size = plugin->size;

    return TRUE;
}

static gboolean _otuswav_start(GstBaseSrc *src) {
    OtusWav *plugin = _OTUSWAV(src);

    GST_DEBUG_OBJECT(plugin, "start");

    FILE *fd = fopen(plugin->location, "r");
    if (NULL == fd) {
        GST_ERROR_OBJECT(plugin, "File '%s' cann't be openned", plugin->location);

        return FALSE; // @todo: or may be exit?
    }

    fseek(fd, 0L, SEEK_END);
    plugin->size = ftell(fd);
    fseek(fd, 0L, SEEK_SET);

    wav_header *header = malloc(sizeof(wav_header));
    if (1 != fread(header, sizeof(wav_header), 1, fd) && ferror(fd)) {
        GST_ERROR_OBJECT(plugin, "File '%s' not wav", plugin->location);

        return FALSE;
    }

    GST_INFO_OBJECT(plugin, "File %s' is wav", plugin->location);

    plugin->fd = fd;

    return TRUE;
}

static gboolean _otuswav_stop(GstBaseSrc *src) {
    OtusWav *plugin = _OTUSWAV(src);

    GST_DEBUG_OBJECT(plugin, "stop");

    if (NULL == plugin->fd) {
        GST_INFO_OBJECT(plugin, "File descriptor is null (maybe already closed)");

        return TRUE;
    }

    fclose(plugin->fd);
    plugin->fd = NULL;

    return TRUE;
}

// static GstFlowReturn _otuswav_create(GstPushSrc *src, GstBuffer **buf) {
//     OtusWav *plugin = _OTUSWAV(src);

//     GST_DEBUG_OBJECT(plugin, "_otuswav_create");

//     *buf = malloc(sizeof(GstBuffer));

//     return GST_FLOW_OK;
// }

// static gboolean _otuswav_negotiate(GstBaseSrc* src) {
//     OtusWav *otuswav = _OTUSWAV(src);

//     GST_DEBUG_OBJECT(otuswav, "negotiate");

//     return TRUE;
// }

static GstFlowReturn _otuswav_fill(GstPushSrc *src, GstBuffer *buf) {
    OtusWav *plugin = _OTUSWAV(src);

    GST_WARNING_OBJECT(plugin, "fill, %p, %lu", (void *)plugin->fd, gst_buffer_get_size(buf));

    if (GST_CLOCK_TIME_IS_VALID (GST_BUFFER_TIMESTAMP (buf))) {
        gst_object_sync_values (GST_OBJECT (plugin), GST_BUFFER_TIMESTAMP (buf));
    }

    gsize size = gst_buffer_get_size(buf);
    guint8 *data = (guint8 *)g_malloc(size);
    size_t read_size;
    if (size != (read_size = fread(data, 1, size, plugin->fd)) && ferror(plugin->fd)) {
        g_free(data);
        return GST_FLOW_ERROR;
    }

    if (feof(plugin->fd)) {
        g_free(data);
        return GST_FLOW_EOS;
    }

    gst_buffer_fill(buf, 0, data, read_size);
    plugin->offset += read_size;

    return GST_FLOW_OK;
}

static gboolean plugin_init(GstPlugin* plugin) {
    return gst_element_register(plugin, "otuswav", GST_RANK_NONE, _OTUSWAV_TYPE);
}

#ifndef VERSION
#define VERSION "0.0.1"
#endif
#ifndef PACKAGE
#define PACKAGE "otuswav"
#endif
#ifndef PACKAGE_NAME
#define PACKAGE_NAME "otuswav"
#endif
#ifndef GST_PACKAGE_ORIGIN
#define GST_PACKAGE_ORIGIN "https://gitlab.com/funkill/"
#endif

GST_PLUGIN_DEFINE(GST_VERSION_MAJOR, GST_VERSION_MINOR, otuswav,
                  "Plays wav audio files", plugin_init, VERSION,
                  GST_LICENSE_UNKNOWN, PACKAGE_NAME, GST_PACKAGE_ORIGIN)
