#ifndef __WAV_H
#define __WAV_H

#include <inttypes.h>

typedef struct wav_header_t {
    uint32_t chunk_id; // 0x52494646
    uint32_t chunk_size;
    uint32_t format; // 0x57415645
    uint32_t subchunk_1_id; //0x666d7420
    uint32_t subchunk_1_size;
    uint16_t audio_format;
    uint16_t num_channels;
    uint32_t sample_rate;
    uint32_t byte_rate;
    uint16_t block_align;
    uint16_t bits_per_sample;
    uint32_t subchunk_2_id; // 0x64617461
    uint32_t subchunk_2_size;
    // char *data;
} wav_header;

#endif
